program PPDBTool;

{$WEAKLINKRTTI ON}
{$RTTI EXPLICIT METHODS([]) PROPERTIES([]) FIELDS([])}
uses
  Vcl.Forms,
  UFrmMain in 'Sys\UFrmMain.pas' {FrmMain},
  UFrmBase in 'Common\UFrmBase.pas' {FrmBase},
  UCommon in 'Common\UCommon.pas',
  UBrMessagebox in 'Common\UBrMessagebox.pas',
  UFamConnTree in 'Sys\UFamConnTree.pas' {FamConnTree: TFrame},
  UDM in 'Sys\UDM.pas' {DM: TDataModule},
  UFrmConnEditor in 'Sys\UFrmConnEditor.pas' {FrmConnEditor},
  UFrmAbout in 'Sys\UFrmAbout.pas' {FrmAbout},
  UFamSqlEdit in 'Editer\UFamSqlEdit.pas' {FamSqlEdit: TFrame},
  UFrmSqlEdit in 'Editer\UFrmSqlEdit.pas' {FrmSqlEdit},
  UFrmTableEdit in 'Editer\UFrmTableEdit.pas' {FrmTableEdit},
  UdxDockEx in 'Common\UdxDockEx.pas',
  UFrmSetConn in 'Sys\UFrmSetConn.pas' {FrmSetConn},
  UMods in 'Sys\UMods.pas',
  UFrmDataEdit in 'Editer\UFrmDataEdit.pas' {FrmDataEdit};

{$R *.res}

begin
  Application.Initialize;
  Application.MainFormOnTaskbar := True;
  Application.CreateForm(TDM, DM);
  Application.CreateForm(TFrmMain, FrmMain);
  //Application.CreateForm(TFrmMainBase, FrmMainBase);
  Application.Run;
end.
