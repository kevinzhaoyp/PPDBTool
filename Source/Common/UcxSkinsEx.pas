{
cxSkinsEx
动态加载皮肤单元, 需要引用单元
1.首先利用dxSkinEditor.exe制作你所需要的皮肤资源文件，最终得到资源文件集合x.skinres
2.利用如下两个函数
dxSkinsUserSkinPopulateSkinNames
dxSkinsUserSkinLoadFromFile
来完成资源文件调用，实现窗体皮肤变更。它们在单元dxSkinsDefaultPainters.pas中；
[@more@] 举例：
第一步，载入皮肤资源文件
cxComboBox1.Properties.Items.Clear;
dxSkinsUserSkinPopulateSkinNames(ExtractFilePath(ParamStr(0))+'bs.skinres', cxComboBox1.Properties.Items);
cxComboBox1.ItemIndex:=0;
第二步，调用皮肤
dxSkinController1.SkinName:='UserSkin';
dxSkinsUserSkinLoadFromFile(ExtractFilePath(ParamStr(0))+'bs.skinres',cxComboBox1.EditText);
dxSkinController1.NativeStyle:=False;
dxSkinController1.UseSkins:=True;

procedure TForm1.dxSkinController1SkinForm(Sender: TObject;
AForm: TCustomForm; var ASkinName: String; var UseSkin: Boolean);
begin
UseSkin:=false;
end;
//dll 皮肤问题
引用  dxCore ,窗体上加 dxskin控件
initialization
  dxInitialize;
finalization
  dxFinalize;
}
unit UcxSkinsEx;

interface

uses
  Vcl.Forms, System.SysUtils, Vcl.Graphics,
  dxSkinsDefaultPainters,
  //
  dxSkinInfo, dxSkinsCore, dxSkinsForm, cxLookAndFeelPainters, cxLookAndFeels,
  dxSkinChooserGallery, dxLayoutLookAndFeels;
  //UserSkin
const
  con_SkinFile = '\Skins\AllSkins.skinres';//目前先使用单个文件
  //ini文件节名与标识名
  con_IniSen = 'UserConfig';
  con_IniIdent = 'Skin';

var
  dxSkin: TdxSkinController;
  dxLayoutSkin: TdxLayoutSkinLookAndFeel; //单元dxLayoutLookAndFeels

  var_SkinName: string;//当前Skin名称
{以下两个过程 只有exe调用，dll不需要调用 2016-12-22 因要加Dll改的}
procedure dxSkinInit;//初始化皮肤控件

{LoadSkinByName  加载皮肤，，-1 文件不存在，1成功，0失败}
function LoadSkinByName(ASkinName: string): Integer;
//保存到配制文件
procedure SaveSkinToIni(ASkinName: string);

procedure SetSkinColor(AColor: TColor; AName: string);  //这个不起作用，不知道为什么

function GetSkinColor(AName: string): TColor;

implementation
uses
  UCommon;

procedure dxSkinInit;//初始化皮肤控件
begin
  dxSkin := TdxSkinController.Create(Application); //交给 Application 来释放
  var_SkinName := AppConfig.ReadString(con_IniSen, con_IniIdent, 'Blue');
  LoadSkinByName(var_SkinName);
  with dxSkin do
  begin
    SkinName := 'UserSkin';
    NativeStyle := False;
    UseSkins := var_SkinName <> '';
  end;
  dxLayoutSkin := TdxLayoutSkinLookAndFeel.Create(Application);
end;

function LoadSkinByName(ASkinName: string): Integer;
var
  aFileName: string;
begin  //-1 文件不存在，1成功，0失败
  aFileName := ExtractFileDir(Application.ExeName) + con_SkinFile;
  if not FileExists(aFileName) then Exit(-1);
  var_SkinName := ASkinName;
  if dxSkinsUserSkinLoadFromFile(aFileName, var_SkinName) then
    Result := 1
  else
    Result := 0;
end;

procedure SaveSkinToIni(ASkinName: string);
begin
  AppConfig.WriteString(con_IniSen, con_IniIdent, ASkinName);
end;

procedure SetSkinColor(AColor: TColor; AName: string);
var
  APainter: TcxCustomLookAndFeelPainter;
  ASkinInfo: TdxSkinInfo;
  ASkin: TdxSkin;
begin
  if cxLookAndFeelPaintersManager.GetPainter('UserSkin', APainter) then
  begin
    APainter.GetPainterData(ASkinInfo);
    ASkin := ASkinInfo.Skin;
    try
      AColor := ASkin.GetColorByName(AName).Value;
    except 
      raise Exception.Create(Format('皮肤文件缺少属性%s', [AName]));
    end;
  end;
end;

function GetSkinColor(AName: string): TColor;
var
  APainter: TcxCustomLookAndFeelPainter;
  ASkinInfo: TdxSkinInfo;
  ASkin: TdxSkin;
begin
  if cxLookAndFeelPaintersManager.GetPainter('UserSkin', APainter) then
  begin
    APainter.GetPainterData(ASkinInfo);
    ASkin := ASkinInfo.Skin;
    try
      Result := ASkin.GetColorByName(AName).Value;
    except 
     // raise Exception.Create(Format('皮肤文件缺少属性%s', [AName]));
    end;
  end;
end;

initialization

finalization
  

end.
