{
  Delphi 2007以上版本
  单元名称：公用共享单元(此单位只需引用即可使用单元中所有对象)
  要改名为接口单元。
  作    者：Kevin
  包括的功能：
  ShellExecute的返回值
  *Delphi控件扩展，如果多可以提到另一个单元好管理。*
  *窗体管理 转到 TFrmBase中
  由于该单元需要其它 非exe工程引用，所以将 initialization 部分初始化功能移动到UDM中。
}

{$WARN SYMBOL_PLATFORM OFF}// 消除Symbol platform的Warning
unit UCommon;

interface

uses
  Windows, Forms, SysUtils, Classes, IniFiles,
  Variants, DateUtils, Controls, StrUtils, ShellAPI,
  Registry, ActiveX, IOUtils;

type
  UserError = class(Exception); // 用户抛出的错误

  TSysConfig = record
    RegInfo: Integer;
    Path: string;
    TempPath: string;
    Title: string;
    HomePage: string;
    ConfigFileName: string;
  public
    procedure LoadConfig(FileName: string);
  end;

  { 窗体类扩展 }
  TFormHelper = class helper for TForm
  published
    procedure SaveStateToIni;
    procedure LoadStateFromIni; // 加载窗体显示位置
    procedure SetMinSize(AWidth, AHeight: Integer); // 设置最小窗体大小(工作区)
  end;

{获取Guid，返回String AType 0带符号38位，1不带大括号36位，2不带符号32位 }
function GetGUID(AType: ShortInt = 2): string;

{ 注册/反注册文件关联 }
function RegisterFileRelation(ExtName, AppName, AppKey, Icon,
  Describe: string): Boolean;

{ 系统全局变量、过程、函数 }
var
  Sys: TSysConfig;
  AppConfig: TIniFile; // Ini配制文件
  AppConfigFile: string; // 系统配制文件默认 ExeName.ini（可以设置）

  { 气泡提示 接UBalloonHint单元如果不加BalloonHint单元没有气泡效果 }
type
  { 气泡提示 过程指针 }
  PShowBalloonTip = procedure(AWC: TWinControl; ATip, ATitle: PWideChar; AIcon: Integer = 1);
  //PShowError = function(AHint: string; AErrorList: string = ''): Integer;
  PEncryption = function(ASource: string): string; //加密 解密 过程
  //对话框
  PPPDialog = function(AText, ACaption: string; Flags: Longint): Integer;
var
  FSBTip: PShowBalloonTip;
  FEncStr: PEncryption;
  FDecStr: PEncryption;
  FPPDialog: PPPDialog;

procedure ShowBalloonTip(AWC: TWinControl; ATip, ATitle: PWideChar; AIcon: Integer = 1);
function EncStr(ASource: string): string;
function DecStr(ASource: string): string;

{ 汉字转拼音首字母 }
function HzToPySzm(const CnString: string): string;
{ 对话框显示 }
procedure ShowMesBox(AText: string); overload;
procedure ShowMesBox(const Fmt: string; const Args: array of const); overload;
procedure ShowMesBox(AHW: HWND; AText: string); overload;
function ShowMesBox(AText: string; Flags: Longint; ACaption: string = ''): Integer; overload;
{ 文件/夹操作类过程函数}
function SearchFile(AFList: TStrings; AFPath, AFExt: string; AType: Integer = 3): Boolean;
function GetFileSizeByName(AFileName: string): Int64;
function FileMoveWithPath(OldFile, NewFile: string): Boolean; // 移动文件，自动创建目录
function FileCopyWithPath(OldFile, NewFile: string): Boolean; // 复制文件，自动创建目录
function RemovePath(FPath: string; FRecycle: Boolean = False): Boolean;
// 删除目录带子目录
{ var变体型处理 对Variants单元的增加补充 }
function VarToInt(const v: Variant; ADef: Integer = 0): Integer;
function VarToDou(const v: Variant; ADef: Double = 0): Double;
function VarToBoo(const v: Variant): Boolean;
function VarToDat(const v: Variant): TDateTime;
function VarToStr(const v: Variant; ADef: string = ''): string;
{ OCX操作 }
{ RegOCX 注册或卸载OCX，Areg=True注册。False卸载 }
function RegOCX(AFile: string; AReg: Boolean = True): Boolean;
function IsRegOCX(const Aclsid: TCLSID): Boolean;
{ iif }
function IIF(AExp: Boolean; ATrue, AFalse: string): string; overload;
function IIF(AExp: Boolean; ATrue, AFalse: Integer): Integer; overload;
function IIF(AExp: Boolean; ATrue, AFalse: Variant): Variant; overload;
{字符列表处理}
function SplitStr(AStr: string; ASplitChar: Char = ';'): TStrings; overload;
procedure SplitStr(AList: TStrings; AStr: string; ASChar: Char = ';'); overload;

function QStr(AValue: Variant; ANULL: Boolean = True): string;
{系统异常处理}
procedure SetAppException; //调用该过程系统就会有异常处理功能。
procedure AppException(Self, Sender: TObject; E: Exception); //错误捕捉事件,在App上

implementation

function QStr(AValue: Variant; ANULL: Boolean = True): string;
begin
  if ANULL and ((AValue = null) or (VarToStrDef(AValue, '') = '')) then
    Result := 'Null'
  else if VarType(AValue) = varDate then
    Result := QuotedStr(FormatDateTime('yyyy-mm-dd', AValue))
  else
    Result := QuotedStr(VarToStrDef(AValue, ''));
end;
{系统异常处理}

procedure SetAppException;
var
  nMethod: TExceptionEvent;
begin
  TMethod(nMethod).Code := @AppException;
  TMethod(nMethod).Data := Application;
  Application.OnException := nMethod;
  {类中的函数（方法）和普通过程的区别就是多一个隐藏的参数Self(TMethod.Data)
  一个方法类型占用8个字节，前4个字节保存函数入口地址，后4个字节保存类实例（Self）
  指针，这就是普通过程和类方法的区别 }
end;

procedure AppException(Self, Sender: TObject; E: Exception);
begin
  ShowMesBox(e.Message);
end;

/// ////////////////////////////////////////////////////////////////////////////////////
function GetGUID(AType: ShortInt): string;
var
  TmpGUID: TGUID;
begin { AType 0带符号38位，1不带大括号36位，2不带符号32位 uses ActiveX,SysUtils,StrUtils }
  if CoCreateGUID(TmpGUID) = S_OK then
    Result := GUIDToString(TmpGUID);
  if AType in [1, 2] then
    Result := Copy(Result, 2, 36); // 去掉两头的大括号
  if AType = 2 then
    Result := AnsiReplaceStr(Result, '-', ''); //.Replace('-','');
end;

{ 气泡提示 接产BalloonHint单元 }
procedure ShowBalloonTip(AWC: TWinControl; ATip, ATitle: PWideChar;
  AIcon: Integer);
begin
  if Assigned(FSBTip) then
    FSBTip(AWC, ATip, ATitle, AIcon)
  else
    ShowMesBox(ATip);
end;
{加密}
function EncStr(ASource: string): string;
begin
  if Assigned(FEncStr) then
    Result := FEncStr(ASource)
  else
    Result := ASource;
end;
{解密}
function DecStr(ASource: string): string;
begin
  if Assigned(FDecStr) then
    Result := FDecStr(ASource)
  else
    Result := ASource;
end;

{ 汉字转拼音首字母 }
function HzToPySzm(const CnString: string): string;
const // 返回中文的拼音首字母
  ChinaCode: array [0 .. 25, 0 .. 1] of Integer = ((1601, 1636), (1637, 1832),
    (1833, 2077), (2078, 2273), (2274, 2301), (2302, 2432), (2433, 2593),
    (2594, 2786), (9999, 0000), (2787, 3105), (3106, 3211), (3212, 3471),
    (3472, 3634), (3635, 3722), (3723, 3729), (3730, 3857), (3858, 4026),
    (4027, 4085), (4086, 4389), (4390, 4557), (9999, 0000), (9999, 0000),
    (4558, 4683), (4684, 4924), (4925, 5248), (5249, 5589));
var
  i, j, HzOrd, l_iHz: Integer;
  Hz: AnsiString;
begin
  i := 1;
  l_iHz := 1;
  while i <= Length(CnString) do
  begin
    Hz := AnsiString(CnString[i]);
    if (Hz[l_iHz] >= #160) and (Hz[l_iHz + 1] >= #160) then
    begin
      HzOrd := (Ord(Hz[l_iHz]) - 160) * 100 + Ord(Hz[l_iHz + 1]) - 160;
      for j := 0 to 25 do
      begin
        if (HzOrd >= ChinaCode[j][0]) and (HzOrd <= ChinaCode[j][1]) then
        begin
          Result := Result + Char(Byte('A') + j); // 改A为a就全部是小写
          Break;
        end;
      end;
    end
    else
      Result := Result + CnString[i];
    Inc(i);
  end;
end;

{ 对话框显示 }
procedure ShowMesBox(AText: string); overload;
begin
  if Assigned(FPPDialog) then
    FPPDialog(AText, '提示', MB_OK + MB_ICONINFORMATION + MB_TOPMOST)
  else
    Application.Messagebox(PChar(AText), '提示', 0 + MB_TOPMOST);
end;

procedure ShowMesBox(const Fmt: string; const Args: array of const); overload;
begin
  ShowMesBox(Format(Fmt, Args));
end;

procedure ShowMesBox(AHW: HWND; AText: string); overload;
begin
  if Assigned(FPPDialog) then
    FPPDialog(AText, Application.Title, MB_OK + MB_TOPMOST)
  else
    Messagebox(AHW, PChar(AText), '', MB_OK + MB_TOPMOST);
end;

function ShowMesBox(AText: string; Flags: Longint; ACaption: string = '')
  : Integer; overload;
begin
  if ACaption = '' then
    ACaption := Application.Title;
  if Assigned(FPPDialog) then
    Result := FPPDialog(AText, ACaption, Flags + MB_TOPMOST)
  else
    Result := Messagebox(Application.Handle, PChar(AText), PChar(ACaption),
      Flags + MB_TOPMOST);
end;

{ 文件/夹操作类过程函数 }

function SearchFile(AFList: TStrings; AFPath, AFExt: string; AType: Integer)
  : Boolean; { AType 1,全部，２全部没有扩展名，３只有名称，４只有名称带扩展名 }
var // 取目录下的SQL文件到列表Astrs
  SearchRec: TSearchRec;
  found: Integer;
  Fstr: string;
begin
  Result := True;
  try
    found := FindFirst(AFPath + '*.' + AFExt, $00000020, SearchRec);
    while found = 0 do
    begin
      case AType of
        1:
          Fstr := AFPath + SearchRec.Name;
        2:
          Fstr := AFPath + ChangeFileExt(SearchRec.Name, '');
        3:
          Fstr := ChangeFileExt(SearchRec.Name, '');
        4:
          Fstr := SearchRec.Name;
      end;
      AFList.Append(Fstr);
      found := FindNext(SearchRec);
    end;
    SysUtils.FindClose(SearchRec);
  except
    Result := False;
  end;
end;

function GetFileSizeByName(AFileName: string): Int64;
var
  aFS: TFileStream;
begin
  Result := 0;
  if not FileExists(AFileName) then
    Exit;
  aFS := TFileStream.Create(AFileName, fmShareDenyNone);
  try
    Result := aFS.Size;
  finally
    aFS.Free;
  end;
end;

function FileMoveWithPath(OldFile, NewFile: string): Boolean;
var
  NewPath: string;
begin
  NewPath := ExtractFilePath(NewFile);
  if not DirectoryExists(NewPath) then
    ForceDirectories(NewPath);
  Result := MoveFile(PWideChar(OldFile), PWideChar(NewFile));
end;

function FileCopyWithPath(OldFile, NewFile: string): Boolean;
var
  NewPath: string;
begin
  NewPath := ExtractFilePath(NewFile);
  if not DirectoryExists(NewPath) then
    ForceDirectories(NewPath);
  Result := CopyFile(PWideChar(OldFile), PWideChar(NewFile), False);
end;

function RemovePath(FPath: string; FRecycle: Boolean): Boolean;
var
  OpStruc: TSHFileOpStruct;
begin
  try
    if FPath[Length(FPath)] = '\' then
      FPath := Copy(FPath, 1, Length(FPath) - 1);

    OpStruc.Wnd := 0;
    OpStruc.wFunc := FO_DELETE;
    OpStruc.pFrom := PChar(FPath + #0);
    OpStruc.pTo := nil;
    // FOF_ALLOWUNDO 回收站
    if FRecycle then
      OpStruc.fFlags := FOF_ALLOWUNDO or FOF_NOCONFIRMATION or FOF_NOERRORUI or
        FOF_SILENT
    else
      OpStruc.fFlags := FOF_NOCONFIRMATION or FOF_NOERRORUI or FOF_SILENT;
    OpStruc.hNameMappings := nil;
    OpStruc.lpszProgressTitle := nil;
    OpStruc.fAnyOperationsAborted := True;

    SHFileOperation(OpStruc); // 在Win7下调试有问题，运行没有问题
    Result := True;
  except
    Result := False;
  end;
end;

{ var变体型处理 }

function VarToInt(const v: Variant; ADef: Integer): Integer;
begin
  try
    if not VarIsNull(v) then
      Result := v
    else
      Result := ADef;
  except
    Result := ADef;
  end;
end;

function VarToDou(const v: Variant; ADef: Double): Double;
begin
  try
    if (not VarIsNull(v)) and (Trim(VarToStrDef(v, '')) <> '') then
      Result := v
    else
      Result := ADef;
  except
    Result := ADef;
  end;
end;

function VarToBoo(const v: Variant): Boolean;
begin
  try
    if VarIsNull(v) then
      Result := False
    else
      Result := v;
  except
    Result := False;
  end;
end;

function VarToDat(const v: Variant): TDateTime;
begin
  try
    if VarIsNull(v) then
      Result := dateof(Date)
    else
      Result := dateof(VarToDateTime(v));
  except
    Result := dateof(Date);
  end;
end;

function VarToStr(const v: Variant; ADef: string = ''): string;
begin
  Result := vartostrdef(v, ADef);
end;

{ 注册/反注册文件关联 }
{
  ExtName, 要检测的扩展名（如.txt）
  AppName, 要关联的应用程序名（如C:\**.exe）
  AppKey,  ExeName扩展名在注册表中的键值（如 txtfile）
  Icon,   扩展名为ExeName的图标文件（如c:\**.ico）
  Describe 说明
}
function RegisterFileRelation(ExtName, AppName, AppKey, Icon,
  Describe: string): Boolean;
var
  AReg: TRegistry;
begin
  AReg := TRegistry.Create;
  try
    try
      AReg.RootKey := HKEY_CLASSES_ROOT;
      AReg.OpenKey(ExtName, True);
      AReg.WriteString('', AppKey);
      AReg.CloseKey;
      AReg.OpenKey(AppKey, True);
      AReg.WriteString('', Describe);
      AReg.CloseKey;
      Result := True;
    except
      Result := False;
    end;
  finally
    AReg.Free;
  end;
end;

{ TFormHelper }

procedure TFormHelper.LoadStateFromIni;
begin // 0 wsNormal, 1 wsMinimized, 2 wsMaximized
  Self.WindowState := TWindowState(AppConfig.ReadInteger(Self.Name,
    'WindowState', Ord(Self.WindowState)));
  if Self.WindowState = wsMinimized then
    Self.WindowState := wsNormal;
  if Self.WindowState <> wsNormal then
    Exit; { //如果是 }

  Self.Top := AppConfig.ReadInteger(Self.Name, 'Top', Self.Top);
  Self.Left := AppConfig.ReadInteger(Self.Name, 'Left', Self.Left);
  Self.ClientWidth := AppConfig.ReadInteger(Self.Name, 'Width', Self.Width);
  Self.ClientHeight := AppConfig.ReadInteger(Self.Name, 'Height', Self.Height);
end;

procedure TFormHelper.SaveStateToIni;
begin
  AppConfig.WriteInteger(Self.Name, 'WindowState', Ord(Self.WindowState));
  if Self.WindowState <> wsNormal then
    Exit;

  AppConfig.WriteInteger(Self.Name, 'Top', Self.Top);
  AppConfig.WriteInteger(Self.Name, 'Left', Self.Left);
  AppConfig.WriteInteger(Self.Name, 'Width', Self.ClientWidth);
  AppConfig.WriteInteger(Self.Name, 'Height', Self.Height);
end;

procedure TFormHelper.SetMinSize(AWidth, AHeight: Integer);
begin
  Self.Constraints.MinHeight := AHeight;
  Self.Constraints.MinWidth := AWidth;
end;

{ OCX操作 }
{ RegOCX 注册或卸载OCX，Areg=True注册。False卸载 }

function RegOCX(AFile: string; AReg: Boolean = True): Boolean;
type
  TOleRegisterFunction = function: HResult; // 注册或卸载函数的原型
var
  aLibH: THandle; // 由LoadLibrary返回的OCX句柄
  aFunctionAddress: TFarProc; // OCX中的函数句柄，由GetProcAddress返回
  aRegFunction: TOleRegisterFunction; // 注册或卸载函数指针
begin
  Result := False;
  // 打开OCX文件，返回的OCX句柄
  aLibH := LoadLibrary(PChar(AFile));
  if aLibH <= 0 then
    Exit; { //OCX句柄正确 }
  try
    // 返回注册或卸载函数的指针
    if AReg then // 返回注册函数的指针
      aFunctionAddress := GetProcAddress(aLibH, PChar('DllRegisterServer'))
    else // 返回卸载函数的指针
      aFunctionAddress := GetProcAddress(aLibH, PChar('DllUnregisterServer'));
    if Assigned(aFunctionAddress) then
    begin // 注册或卸载函数存在
      aRegFunction := TOleRegisterFunction(aFunctionAddress); // 获取操作函数的指针
      Result := aRegFunction >= 0; // 执行注册或卸载操作，返回值> =0表示执行成功
    end
    else
      RaiseLastWin32Error;
  finally
    FreeLibrary(aLibH); //
  end;
end;

function IsRegOCX(const Aclsid: TCLSID): Boolean;

var
  OleStr: POleStr;
  reg: TRegInifile;
  Key, FileName: string;
begin
  Result := ProgIDFromCLSID(Aclsid, OleStr) = S_OK;
  if not Result then
    Exit;
  Key := Format('\SOFTWARE\Classes\CLSID\%s', [GUIDToString(Aclsid)]);
  reg := TRegInifile.Create;
  try
    reg.RootKey := HKEY_LOCAL_MACHINE;
    Result := reg.OpenKeyReadOnly(Key);
    if not Result then
      Exit;
    FileName := reg.ReadString('InProcServer32', '', EmptyStr);
    if (FileName = EmptyStr) then
    begin
      FileName := reg.ReadString('InProcServer', '', EmptyStr);
    end;
    Result := FileName <> EmptyStr;
    if not Result then
      Exit;
    Result := FileExists(FileName);
  finally
    reg.Free;
  end;
end;

{ iif }
function IIF(AExp: Boolean; ATrue, AFalse: string): string;
begin
  if AExp then
    Result := ATrue
  else
    Result := AFalse;
end;

function IIF(AExp: Boolean; ATrue, AFalse: Integer): Integer;
begin
  if AExp then
    Result := ATrue
  else
    Result := AFalse;
end;

function IIF(AExp: Boolean; ATrue, AFalse: Variant): Variant;
begin
  if AExp then
    Result := ATrue
  else
    Result := AFalse;
end;

function SplitStr(AStr: string; ASplitChar: Char = ';'): TStrings;
begin
  Result := TStringList.Create;
  SplitStr(Result, AStr, ASplitChar);
end;

procedure SplitStr(AList: TStrings; AStr: string; ASChar: Char = ';');
begin
{StrictDelimiter  2009及以后才有。
将严格按照预先设定的分隔符去分隔字符．　
反之将会使用设定分隔符，空格，tab. 之类不可见的字符同时对字符做分隔 }
  if Trim(AStr) = '' then Exit;
  AList.BeginUpdate;
  try
    AList.StrictDelimiter := True;
    AList.Delimiter := ASChar;
    AList.DelimitedText:= AStr;
    ////删除最后一行空行
    if (AList.Strings[AList.Count - 1]) = '' then
      AList.Delete(AList.Count - 1);
  finally
    AList.EndUpdate;
  end;
end;

{ 自动处理 }
{ TSysConfig }

procedure TSysConfig.LoadConfig(FileName: string);
begin
  Self.ConfigFileName := FileName;
  with TIniFile.Create(FileName) do
  begin
    Self.Title := ReadString('Application', 'Title', Application.Title);
    Self.HomePage := ReadString('Application', 'HomePage', '');
    Free;
  end;
end;

initialization

SetAppException;

Sys.Path := ExtractFileDir(Application.ExeName) + '\';
Sys.TempPath := Sys.Path + 'temp\';
Sys.LoadConfig(Sys.Path + 'Config\SysConfig.ini');
//创建临时目录
if Trim(Sys.TempPath) <> '' then
begin
  if not DirectoryExists(Sys.TempPath) then
    ForceDirectories(Sys.TempPath); // CreateDirectory()
  FileSetAttr(Sys.TempPath, faHidden); // 设置隐藏文件夹
end;

AppConfigFile := ChangeFileExt(ExtractFileName(Application.ExeName), '.ini');
Application.HelpFile := Sys.Path + ChangeFileExt
  (ExtractFileName(Application.ExeName), '.chm');
AppConfig := TIniFile.Create(Sys.Path + AppConfigFile);

Application.Title := Sys.Title;
Application.ShowHint := True;

finalization

FreeAndNil(AppConfig);
//
if TDirectory.Exists(Sys.TempPath) then
  TDirectory.Delete(Sys.TempPath, True); // IOUtils

end.
