{*******************************************************}
{                                                       }
{       系统名称 自定义消息框                           }
{       版权所有                                        }
{       单元功能 消息框                                 }
{                                                       }
{*******************************************************}
unit UBrMessagebox;

interface

uses
  Classes, Windows, Controls, Forms, Dialogs, StdCtrls, SysUtils, Graphics, Math, ExtCtrls;


  function CreateBrMessageDialog(const Msg,MsgTitle: string; DlgType: TMsgDlgType;Buttons: TMsgDlgButtons): TForm;

  function MessageBrDlg( Msg,MsgTitle: string; Flags: Longint): Integer;
implementation

uses UCommon;

type
  TMessageForm = class(TForm)
  private
    Message: TLabel;
    procedure HelpButtonClick(Sender: TObject);
  protected
    procedure CustomKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure WriteToClipBoard(Text: String);
    function GetFormText: String;
  public
    constructor CreateNew(AOwner: TComponent); reintroduce;
  end;

procedure Beep;
begin
  MessageBeep(0);
end;

constructor TMessageForm.CreateNew(AOwner: TComponent);
var
  NonClientMetrics: TNonClientMetrics;
begin
  inherited CreateNew(AOwner);
  NonClientMetrics.cbSize := sizeof(NonClientMetrics);
  if SystemParametersInfo(SPI_GETNONCLIENTMETRICS, 0, @NonClientMetrics, 0) then
    Font.Handle := CreateFontIndirect(NonClientMetrics.lfMessageFont);
end;

procedure TMessageForm.HelpButtonClick(Sender: TObject);
begin
  Application.HelpContext(HelpContext);
end;

procedure TMessageForm.CustomKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
begin
  if (Shift = [ssCtrl]) and (Key = Word('C')) then
  begin
    Beep;
    WriteToClipBoard(GetFormText);
  end;
end;

procedure TMessageForm.WriteToClipBoard(Text: String);
var
  Data: THandle;
  DataPtr: Pointer;
begin
 if OpenClipBoard(0) then
 begin
   try
     Data := GlobalAlloc(GMEM_MOVEABLE+GMEM_DDESHARE, Length(Text) + 1);
     try
       DataPtr := GlobalLock(Data);
       try
         Move(PChar(Text)^, DataPtr^, Length(Text) + 1);
         EmptyClipBoard;
         SetClipboardData(CF_TEXT, Data);
       finally
         GlobalUnlock(Data);
       end;
      except
        GlobalFree(Data);
        raise;
      end;
    finally
      CloseClipBoard;
    end;
  end
  else
    raise Exception.Create('Cannot open clipboard');
end;

function TMessageForm.GetFormText: String;
var
  DividerLine, ButtonCaptions: string;
  I: integer;
begin
  DividerLine := StringOfChar('-', 27) + sLineBreak;
  for I := 0 to ComponentCount - 1 do
    if Components[I] is TButton then
      ButtonCaptions := ButtonCaptions + TButton(Components[I]).Caption +
        StringOfChar(' ', 3);
  ButtonCaptions := StringReplace(ButtonCaptions,'&','', [rfReplaceAll]);
  Result := Format('%s%s%s%s%s%s%s%s%s%s', [DividerLine, Caption, sLineBreak,
    DividerLine, Message.Caption, sLineBreak, DividerLine, ButtonCaptions,
    sLineBreak, DividerLine]);
end;

function GetAveCharSize(Canvas: TCanvas): TPoint;
var
  I: Integer;
  Buffer: array[0..51] of Char;
begin
  for I := 0 to 25 do Buffer[I] := Chr(I + Ord('A'));
  for I := 0 to 25 do Buffer[I + 26] := Chr(I + Ord('a'));
  GetTextExtentPoint(Canvas.Handle, Buffer, 52, TSize(Result));
  Result.X := Result.X div 52;
end;

var
  IconIDs: array[TMsgDlgType] of PChar = (IDI_EXCLAMATION, IDI_HAND,
    IDI_ASTERISK, IDI_QUESTION, nil);


  ButtonNames: array[TMsgDlgBtn] of string = (
    'Yes', 'No', 'OK', 'Cancel', 'Abort', 'Retry', 'Ignore', 'All', 'NoToAll',
    'YesToAll', 'Help','Close');
    //'ForceClose', 'Continute',
  ButtonCaptions: array[TMsgDlgBtn] of string = (
    '是(&Y)', '否(&N)', '确定', '取消', '终止(&A)', '重试(&R)', '忽略(&I)',
    '全部(&A)', '全否(&O)', '全是(&A)', '帮助(&H)', '关闭(&C)');
    // '强制关闭', '继续',
  ModalResults: array[TMsgDlgBtn] of Integer = (
    mrYes, mrNo, mrOk, mrCancel, mrAbort, mrRetry, mrIgnore, mrAll, mrNoToAll,
    mrYesToAll, 0, mrClose);
    //mrForceClose, mrContinute,
var
  ButtonWidths : array[TMsgDlgBtn] of integer;

function CreateBrMessageDialog(const Msg,MsgTitle: string; DlgType: TMsgDlgType;
  Buttons: TMsgDlgButtons): TForm;
const
  mcHorzMargin = 8;
  mcVertMargin = 8;
  mcHorzSpacing = 10;
  mcVertSpacing = 10;
  mcButtonWidth = 50;
  mcButtonHeight = 14;
  mcButtonSpacing = 4;
var
  DialogUnits: TPoint;
  HorzMargin, VertMargin, HorzSpacing, VertSpacing, ButtonWidth,
  ButtonHeight, ButtonSpacing, ButtonCount, ButtonGroupWidth,
  IconTextWidth, IconTextHeight, X, ALeft: Integer;
  B, DefaultButton, CancelButton: TMsgDlgBtn;
  IconID: PChar;
  TextRect: TRect;
begin
  Result := TMessageForm.CreateNew(Application);
  with Result do
  begin
    BiDiMode := Application.BiDiMode;
    BorderStyle := bsDialog;
    Canvas.Font := Font;
    KeyPreview := True;
    OnKeyDown := TMessageForm(Result).CustomKeyDown;
    DialogUnits := GetAveCharSize(Canvas);
    HorzMargin := MulDiv(mcHorzMargin, DialogUnits.X, 4);
    VertMargin := MulDiv(mcVertMargin, DialogUnits.Y, 8);
    HorzSpacing := MulDiv(mcHorzSpacing, DialogUnits.X, 4);
    VertSpacing := MulDiv(mcVertSpacing, DialogUnits.Y, 8);
    ButtonWidth := MulDiv(mcButtonWidth, DialogUnits.X, 4);
    for B := Low(TMsgDlgBtn) to High(TMsgDlgBtn) do
    begin
      if B in Buttons then
      begin
        if ButtonWidths[B] = 0 then
        begin
          TextRect := Rect(0,0,0,0);
          Windows.DrawText( canvas.handle,
            PChar(ButtonCaptions[B]), -1,
            TextRect, DT_CALCRECT or DT_LEFT or DT_SINGLELINE or
            DrawTextBiDiModeFlagsReadingOnly);
          with TextRect do ButtonWidths[B] := Right - Left + 8;
        end;
        if ButtonWidths[B] > ButtonWidth then
          ButtonWidth := ButtonWidths[B];
      end;
    end;
    ButtonHeight := MulDiv(mcButtonHeight, DialogUnits.Y, 8);
    ButtonSpacing := MulDiv(mcButtonSpacing, DialogUnits.X, 4);
    SetRect(TextRect, 0, 0, Screen.Width div 2,0);
    DrawText(Canvas.Handle, PChar(Msg), Length(Msg)+1, TextRect,
      DT_EXPANDTABS or DT_CALCRECT or DT_WORDBREAK or
      DrawTextBiDiModeFlagsReadingOnly);
    IconID := IconIDs[DlgType];
    IconTextWidth := TextRect.Right;
    IconTextHeight := TextRect.Bottom;
    if IconID <> nil then
    begin
      Inc(IconTextWidth, 32 + HorzSpacing);
      if IconTextHeight < 32 then IconTextHeight := 32;
    end;
    ButtonCount := 0;
    for B := Low(TMsgDlgBtn) to High(TMsgDlgBtn) do
      if B in Buttons then Inc(ButtonCount);
    ButtonGroupWidth := 0;
    if ButtonCount <> 0 then
      ButtonGroupWidth := ButtonWidth * ButtonCount +
        ButtonSpacing * (ButtonCount - 1);
    ClientWidth := Max(IconTextWidth, ButtonGroupWidth) + HorzMargin * 2;
    ClientHeight := IconTextHeight + ButtonHeight + VertSpacing +
      VertMargin * 2;
    Left := (Screen.Width div 2) - (Width div 2);
    Top := (Screen.Height div 2) - (Height div 2);
    Caption := MsgTitle ;
    if IconID <> nil then
      with TImage.Create(Result) do
      begin
        Name := 'Image';
        Parent := Result;
        Picture.Icon.Handle := LoadIcon(0, IconID);
        SetBounds(HorzMargin, VertMargin, 32, 32);
      end;
    TMessageForm(Result).Message := TLabel.Create(Result);
    with TMessageForm(Result).Message do
    begin
      Name := 'Message';
      Parent := Result;
      WordWrap := True;
      Caption := Msg;
      BoundsRect := TextRect;
      BiDiMode := Result.BiDiMode;
      ALeft := IconTextWidth - TextRect.Right + HorzMargin;
      if UseRightToLeftAlignment then
        ALeft := Result.ClientWidth - ALeft - Width;
      SetBounds(ALeft, VertMargin,
        TextRect.Right, TextRect.Bottom);
    end;
    if mbOk in Buttons then DefaultButton := mbOk else
      if mbYes in Buttons then DefaultButton := mbYes else
        DefaultButton := mbRetry;
    if mbCancel in Buttons then CancelButton := mbCancel else
      if mbNo in Buttons then CancelButton := mbNo else
        CancelButton := mbOk;
    X := (ClientWidth - ButtonGroupWidth) div 2;
    for B := Low(TMsgDlgBtn) to High(TMsgDlgBtn) do
      if B in Buttons then
        with TButton.Create(Result) do
        begin
          Name := ButtonNames[B];
          Parent := Result;
          Caption := ButtonCaptions[B];
          ModalResult := ModalResults[B];
          if B = DefaultButton then Default := True;
          if B = CancelButton then Cancel := True;
          SetBounds(X, IconTextHeight + VertMargin + VertSpacing,
            ButtonWidth, ButtonHeight);
          Inc(X, ButtonWidth + ButtonSpacing);
          if B = mbHelp then
            OnClick := TMessageForm(Result).HelpButtonClick;
        end;
  end;
end;

{-------------------------------------------------------------------------------
  过程名:    MessageMyDlg
  功能:      自定义的消息框
  参数:      Msg: string;                  消息内容
             MsgTitle: string;             消息标题
             DlgType: TMsgDlgType;         消息显示图标
             Buttons: TMyMsgDlgButtons;    消息按钮集合
             ShowClose: Boolean            消息框标题栏关闭按钮的显示
  返回值:    Integer
-------------------------------------------------------------------------------}
function MessageBrDlg(Msg,MsgTitle: string;  Flags: Longint): Integer;
var
  ADlgType : TMsgDlgType ;
  AButtons: TMsgDlgButtons ;
  DlgType : TMsgDlgType ;
  ADlgTypeByte ,AButtonsByte : Byte ;
  ShowClose : Boolean ;
  function OcttoBin(i: integer): string;  //十进制转换为二进制字符串
var
  j: integer;
  s: string;
begin
  j := i;
  s := ' ';
  while j >= 2 do
  begin
    if (j mod 2) = 1 then
    begin
      s := '1' + s;
      j := j div 2;
    end
    else
    begin
      s := '0' + s;
      j := j div 2;
    end;
  end;
  s := chr(ord('0') + j) + s;
  result := s;
end;

  procedure FlogToDlgTypeAndDlgButtons(Flags: Longint ; var DlgType : TMsgDlgType;var Buttons: TMsgDlgButtons );
  begin
    Flags := Flags ;

    if (Flags and MB_ICONINFORMATION) = MB_ICONINFORMATION then
        DlgType := mtInformation
    else
      if (Flags and MB_ICONWARNING) = MB_ICONWARNING then
        DlgType := mtWarning
      else
        if (Flags and MB_ICONQUESTION) = MB_ICONQUESTION then
          DlgType := mtConfirmation
        else
          if (Flags and MB_ICONERROR) = MB_ICONERROR then
            DlgType := mtError ;


    if (Flags and MB_OK) = MB_OK then
        Buttons := [mbOK] ;

    if (Flags and MB_OKCANCEL) = MB_OKCANCEL then
        Buttons := [mbOK,mbCancel] ;

    if (Flags and MB_ABORTRETRYIGNORE) = MB_ABORTRETRYIGNORE then
        Buttons := [mbAbort,mbRetry,mbIgnore] ;

    if (Flags and MB_YESNOCANCEL) = MB_YESNOCANCEL then
        Buttons := [mbYes,mbNo,mbCancel] ;

    if (Flags and MB_YESNO) = MB_YESNO then
        Buttons := [mbYes,mbNo] ;

    if (Flags and MB_RETRYCANCEL) = MB_RETRYCANCEL then
        Buttons := [mbRetry,mbCancel] ;

  end ;
begin
  ShowClose := False ;
  FlogToDlgTypeAndDlgButtons(Flags,ADlgType,AButtons);
  with CreateBrMessageDialog(Msg, MsgTitle, ADlgType, AButtons) do
    try
      if ShowClose then
        BorderIcons := BorderIcons + [biSystemMenu]
      else
        BorderIcons := BorderIcons - [biSystemMenu];
      Position := poScreenCenter;
      Result := ShowModal ;
    finally
      Free;
    end;

end;
initialization
  FPPDialog := MessageBrDlg;



end.

