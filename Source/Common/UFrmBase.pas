{
使用cxTabC来管理子窗体
要使用 UMods中的
  ActiveConn: TPPConn; //对象树中的当前链接
  FTabC: TcxTabControl;
}
unit UFrmBase;

interface

uses
  Windows, Messages, Classes, Controls, Forms, ActnList, Graphics, ImgList,
  UDM, cxGraphics, cxControls, cxLookAndFeels, cxLookAndFeelPainters,
  dxSkinsCore, dxSkinBlue, dxSkinSharpPlus, dxSkinSilver,
  dxSkinsdxStatusBarPainter, dxStatusBar, dxSkinOffice2013White,
  dxSkinWhiteprint, System.SysUtils;

type
  TFrmBase = class(TForm)
    dxSBar1: TdxStatusBar;
    procedure ConnAfterConnect(Sender: TObject); virtual;
    procedure ConnAfterDisConnect(Sender: TObject); virtual;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormDestroy(Sender: TObject);
  private
    FConn: TPPConn;
    procedure SetConn(const Value: TPPConn);
  public
    constructor Create(AOwner: TComponent); override;
    property Conn: TPPConn read FConn write SetConn;
  end;

implementation
{$R *.dfm}

procedure TFrmBase.ConnAfterConnect(Sender: TObject);
begin
  // 可能 要根据不周的数据做不同的提示
  dxSBar1.Panels[0].Text := Format('状态：已链接 - 数据库[%s]',
    [TPPConn(Sender).Params.Database]);
  TdxStatusBarTextPanelStyle(dxSBar1.Panels[0].PanelStyle).ImageIndex := 3;
end;

procedure TFrmBase.ConnAfterDisConnect(Sender: TObject);
begin
  dxSBar1.Panels[0].Text := '状态：已断开';
  TdxStatusBarTextPanelStyle(dxSBar1.Panels[0].PanelStyle).ImageIndex := 6;
end;

constructor TFrmBase.Create(AOwner: TComponent);
begin
  inherited Create(AOwner);
  BorderStyle := bsNone;
  Align := alClient;
  //以后可以考虑 窗体显示后在执行这里的代码。
  if Assigned(ActiveConn) then
    Conn := ActiveConn.CloneConnection as TPPConn
  else
    Conn := TPPConn.Create(Self);
end;

procedure TFrmBase.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  Action := caFree;
  Self.Release;
end;

procedure TFrmBase.FormDestroy(Sender: TObject);
begin
  FConn.Free;
end;

procedure TFrmBase.SetConn(const Value: TPPConn);
begin
  FConn := Value;
  FConn.AfterConnect := ConnAfterConnect;
  FConn.AfterDisconnect := ConnAfterDisConnect;
  try
    FConn.Connected := True;
  except
    FConn.ShowConnEdit;
  end;
end;

end.
