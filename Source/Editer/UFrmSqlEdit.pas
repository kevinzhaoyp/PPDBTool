{
access数据库有个隐藏的系统表msysobjects，筛选出type=1和flags=0的就是表名了
}

unit UFrmSqlEdit;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, UFrmBase, cxGraphics, cxControls,
  cxLookAndFeels, cxLookAndFeelPainters, dxSkinsCore, dxSkinBlue, UDM,
  dxSkinSharpPlus, dxSkinSilver, dxSkinscxPCPainter, dxBarBuiltInMenu, cxPC,
  cxSplitter, UFamSqlEdit, dxSkinsdxStatusBarPainter, dxStatusBar,
  dxSkinOffice2013White, dxSkinWhiteprint, cxContainer, cxEdit, cxTextEdit,
  cxMemo, System.Actions, Vcl.ActnList, cxStyles, cxCustomData, cxFilter,
  cxData, cxDataStorage, cxNavigator, Data.DB, cxDBData, cxGridLevel, cxClasses,
  cxGridCustomView, cxGridCustomTableView, cxGridTableView, cxGridDBTableView,
  cxGrid, dxSkinsdxBarPainter, dxBar;

type
  TFrmStyle = (fsScript, fsDataEdit);
  TFrmSqlEdit = class(TFrmBase)
    cxPC1: TcxPageControl;
    cxSplitter1: TcxSplitter;
    cxTabSheet1: TcxTabSheet;
    cxTabSheet2: TcxTabSheet;
    SqlEdit: TFamSqlEdit;
    cxMemo1: TcxMemo;
    ActionList1: TActionList;
    ActOpen: TAction;
    cxGTV1: TcxGridDBTableView;
    cxGrid1Level1: TcxGridLevel;
    cxGrid1: TcxGrid;
    dxBarManager1: TdxBarManager;
    dxBarManager1Bar1: TdxBar;
    dxBarButton2: TdxBarButton;
    dxBarButton5: TdxBarButton;
    actExec: TAction;
    procedure ActOpenUpdate(Sender: TObject);
    procedure ActOpenExecute(Sender: TObject);
    procedure FormKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure actExecExecute(Sender: TObject);
  private
    FFileName: string;
    FFrmStyle: TFrmStyle;//当前文件，如果为空表示新建
    procedure ConnAfterConnect(Sender: TObject); override;
    procedure SetFrmStyle(const Value: TFrmStyle);
  public
    procedure NewSql(ASql: string = '');//新建Sql 查询
    procedure LoadFromFile(AFileName: string); //从文件中打开
    constructor Create(AOwner: TComponent); override;

    property FrmStyle: TFrmStyle read FFrmStyle write SetFrmStyle;
  end;

var
  FrmSqlEdit: TFrmSqlEdit;

implementation
{$R *.dfm}

{ TFrmSqlEdit }

procedure TFrmSqlEdit.actExecExecute(Sender: TObject);
begin
  inherited;
  cxMemo1.Lines.Clear;
  try
    Conn.ExecSQL(SqlEdit.GetCode);
    cxPC1.ActivePageIndex := 1;
  except on e: Exception do
    begin
      cxMemo1.Lines.Add(e.ClassName);
      cxMemo1.Lines.Add(e.Message);
      cxSplitter1.OpenSplitter;
      cxPC1.ActivePageIndex := 1;
      Exit;
    end;
  end;
end;

procedure TFrmSqlEdit.ActOpenExecute(Sender: TObject);
var
  aDSet : TDataSet;
begin //执行
  inherited;
  cxMemo1.Lines.Clear;
  try
    aDSet := Conn.OpenSql(SqlEdit.GetCode);
    if FrmStyle = fsScript then
      cxSplitter1.OpenSplitter;
    cxPC1.ActivePageIndex := 0;
  except on e: Exception do
    begin
      cxMemo1.Lines.Add(e.ClassName);
      cxMemo1.Lines.Add(e.Message);
      cxSplitter1.OpenSplitter;
      cxPC1.ActivePageIndex := 1;
      Exit;
    end;
  end;
  with cxGTV1 do
  begin
    DataController.DataSource.DataSet := aDSet;
    ClearItems;//清空数据
    (DataController as IcxCustomGridDataController).DeleteAllItems;//删除所有列
    (DataController as IcxCustomGridDataController).CreateAllItems(False);//创建数据源中的所有列
    ApplyBestFit;//让列宽自适应.BestFitMaxWidth;
  end;
end;

procedure TFrmSqlEdit.ActOpenUpdate(Sender: TObject);
begin
  inherited;
  TAction(Sender).Enabled := SqlEdit.HasCode;
end;

procedure TFrmSqlEdit.ConnAfterConnect(Sender: TObject);
begin
  inherited;
  Conn.GetTables(SqlEdit.SynSQL.TableNames);//加载所有表

end;

constructor TFrmSqlEdit.Create(AOwner: TComponent);
begin
  inherited Create(AOwner);
  cxGTV1.DataController.DataSource := TDataSource.Create(cxGTV1);
end;

procedure TFrmSqlEdit.FormKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  inherited;
  case Key of
    VK_F5: ActOpen.Execute;
    VK_F6: actExec.Execute;
  end;
end;

procedure TFrmSqlEdit.LoadFromFile(AFileName: string);
begin
  SqlEdit.SQL.LoadFromFile(AFileName);
  FFileName := AFileName;
end;

procedure TFrmSqlEdit.NewSql(ASql: string);
begin
  SqlEdit.SQL.Text := ASql;
  FFileName := '';
end;

procedure TFrmSqlEdit.SetFrmStyle(const Value: TFrmStyle);
begin
  if FFrmStyle = Value then Exit;
  FFrmStyle := Value;

  cxGTV1.Navigator.Visible := Value = fsDataEdit;
  if Value = fsDataEdit then //以Grid为主
  begin
    SqlEdit.Align := alTop;
    SqlEdit.Height := 100;
    cxSplitter1.AlignSplitter := salTop;
    cxSplitter1.Control := SqlEdit;

    cxPC1.Align := alClient;
  end
  else if Value = fsScript then //以SqlEdit为主
  begin
    SqlEdit.Align := alClient;

    cxPC1.Align := alBottom;
    cxSplitter1.AlignSplitter := salBottom;
    cxSplitter1.Control := cxPC1;
  end;
  cxSplitter1.CloseSplitter;//
end;

end.
