inherited FrmEditer: TFrmEditer
  Caption = 'FrmEditer'
  ClientHeight = 395
  ClientWidth = 612
  FormStyle = fsMDIChild
  Visible = True
  ExplicitWidth = 628
  ExplicitHeight = 434
  PixelsPerInch = 96
  TextHeight = 17
  object cxPageControl1: TcxPageControl [0]
    Left = 0
    Top = 204
    Width = 612
    Height = 171
    Align = alBottom
    TabOrder = 0
    Properties.ActivePage = cxTabSheet1
    Properties.CustomButtons.Buttons = <>
    Properties.HotTrack = True
    Properties.Images = DM.cxIL16
    ClientRectBottom = 166
    ClientRectLeft = 5
    ClientRectRight = 607
    ClientRectTop = 31
    object cxTabSheet1: TcxTabSheet
      Caption = #32467#26524
      ImageIndex = 16
    end
    object cxTabSheet2: TcxTabSheet
      Caption = #28040#24687
      ImageIndex = 12
    end
  end
  object cxSplitter1: TcxSplitter [1]
    Left = 0
    Top = 196
    Width = 612
    Height = 8
    AlignSplitter = salBottom
    Control = cxPageControl1
  end
  inline FamSqlEdit1: TFamSqlEdit [2]
    Left = 0
    Top = 0
    Width = 612
    Height = 196
    Align = alClient
    TabOrder = 2
    ExplicitWidth = 612
    ExplicitHeight = 196
    inherited SynEdit1: TSynEdit
      Width = 612
      Height = 196
      ExplicitWidth = 612
      ExplicitHeight = 196
    end
  end
  inherited dxStatusBar1: TdxStatusBar
    Top = 375
    Width = 612
    ExplicitLeft = 0
    ExplicitTop = 375
    ExplicitWidth = 612
  end
end
