unit UFamSqlEdit;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes,
  Vcl.Graphics, Vcl.Controls, Vcl.Forms, Vcl.Dialogs, cxGraphics,
  cxControls, cxLookAndFeels, cxLookAndFeelPainters, cxContainer, cxEdit,
  dxSkinsCore, dxSkinBlue, dxSkinSharpPlus, dxSkinSilver, cxTextEdit, cxMemo,
  SynEditHighlighter, SynHighlighterSQL, SynEdit, SynCompletionProposal,
  System.ImageList, Vcl.ImgList;

type
  TFamSqlEdit = class(TFrame)
    SynEdit1: TSynEdit;
    scp1: TSynCompletionProposal;
    SynSQL: TSynSQLSyn;
    ImageList1: TImageList;
  private
    function GetSQL: TStrings;
    { Private declarations }
  public
    { Public declarations }
    function HasCode: Boolean; //是否有可执行的代码
    function GetCode: string; //获取要执行的代码
  published
    property SQL: TStrings read GetSQL;
  end;

implementation

{$R *.dfm}

{ TFamSqlEdit }

function TFamSqlEdit.GetCode: string;
begin
  Result := SynEdit1.SelText;//先看有没有选中的代码
  if Trim(Result) <> '' then Exit;
  Result := SynEdit1.Text;
end;

function TFamSqlEdit.GetSQL: TStrings;
begin
  Result := SynEdit1.Lines;
end;

function TFamSqlEdit.HasCode: Boolean;
begin
  Result := True;
end;

end.
