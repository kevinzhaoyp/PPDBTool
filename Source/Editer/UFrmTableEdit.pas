unit UFrmTableEdit;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, UFrmBase, cxGraphics, cxControls, UDM,
  cxLookAndFeels, cxLookAndFeelPainters, dxSkinsCore, dxSkinBlue,
  dxSkinSharpPlus, dxSkinSilver, dxSkinsdxStatusBarPainter, dxStatusBar,
  dxSkinscxPCPainter, dxBarBuiltInMenu, cxPC, cxStyles, cxCustomData, cxFilter,
  cxData, cxDataStorage, cxEdit, cxNavigator, Data.DB, cxDBData, cxGridLevel,
  cxClasses, cxGridCustomView, cxGridCustomTableView, cxGridTableView,
  cxGridDBTableView, cxGrid, FireDAC.Stan.Intf, FireDAC.Stan.Option,
  FireDAC.Stan.Param, FireDAC.Stan.Error, FireDAC.DatS, FireDAC.Phys.Intf,
  FireDAC.DApt.Intf, FireDAC.Comp.DataSet, FireDAC.Comp.Client, cxCheckBox,
  dxLayoutContainer, dxLayoutControl, dxLayoutcxEditAdapters, cxContainer,
  cxTextEdit, dxLayoutControlAdapters, Vcl.ExtCtrls, dxSkinOffice2013White,
  dxSkinWhiteprint;

type
  TFrmTableEdit = class(TFrmBase)
    cxGTV1: TcxGridDBTableView;
    cxGrid1Level1: TcxGridLevel;
    cxGrid1: TcxGrid;
    FDMemTable1: TFDMemTable;
    DataSource1: TDataSource;
    cxGTV1FieldName: TcxGridDBColumn;
    cxGTV1FieldType: TcxGridDBColumn;
    cxGTV1FIsNull: TcxGridDBColumn;
    cxGTV1FKey: TcxGridDBColumn;
    dxLayoutControl1Group_Root: TdxLayoutGroup;
    dxLayoutControl1: TdxLayoutControl;
    dxLayoutItem1: TdxLayoutItem;
    cxTextEdit1: TcxTextEdit;
    dxLayoutItem2: TdxLayoutItem;
    Panel1: TPanel;
    dxLayoutItem3: TdxLayoutItem;
    dxLayoutGroup1: TdxLayoutGroup;
  private
  public
    class function FrmShow(AConn: TPPConn; ATableName: string = ''): TFrmBase;
  end;

var
  FrmTableEdit: TFrmTableEdit;

implementation

{$R *.dfm}

{ TFrmTableEdit }

class function TFrmTableEdit.FrmShow(AConn: TPPConn; ATableName: string): TFrmBase;
begin
// ATableName = ''时为新建表
  Result := TFrmTableEdit.Create(Application);
  with Result do
  begin
    BorderStyle := bsNone;
    Align := alClient;
    //Visible := False;
  end;
end;

end.
