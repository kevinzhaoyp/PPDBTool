{
access数据库有个隐藏的系统表msysobjects，筛选出type=1和flags=0的就是表名了
}

unit UFrmEditer;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, UFrmBase, cxGraphics, cxControls,
  cxLookAndFeels, cxLookAndFeelPainters, dxSkinsCore, dxSkinBlue, UDM,
  dxSkinSharpPlus, dxSkinSilver, dxSkinscxPCPainter, dxBarBuiltInMenu, cxPC,
  cxSplitter, UFamSqlEdit, dxSkinsdxStatusBarPainter, dxStatusBar;

type
  TFrmEditer = class(TFrmBase)
    cxPageControl1: TcxPageControl;
    cxSplitter1: TcxSplitter;
    cxTabSheet1: TcxTabSheet;
    cxTabSheet2: TcxTabSheet;
    FamSqlEdit1: TFamSqlEdit;
  private

  public
    { Public declarations }
    class function ShowWithParent(AParent: TWinControl): TForm;
  end;

var
  FrmEditer: TFrmEditer;

implementation
{$R *.dfm}

{ TFrmEditer }

class function TFrmEditer.ShowWithParent(AParent: TWinControl): TForm;
begin
  with TFrmEditer.Create(AParent) do
  begin
    BorderStyle := bsNone;
    Align := alClient;
    Parent := AParent;
    Show;
  end;
end;

end.
