inherited FrmTableEdit: TFrmTableEdit
  Caption = 'FrmTableEdit'
  ClientHeight = 430
  ClientWidth = 655
  ExplicitWidth = 671
  ExplicitHeight = 469
  PixelsPerInch = 96
  TextHeight = 17
  inherited dxSBar1: TdxStatusBar
    Top = 402
    Width = 655
    ExplicitTop = 402
    ExplicitWidth = 655
  end
  object dxLayoutControl1: TdxLayoutControl
    Left = 0
    Top = 0
    Width = 655
    Height = 402
    Align = alClient
    TabOrder = 1
    LayoutLookAndFeel = DM.dxLayoutCxLookAndFeel1
    OptionsImage.Images = DM.cxIL16
    object Panel1: TPanel
      Left = 10000
      Top = 10000
      Width = 185
      Height = 41
      Caption = 'Panel1'
      Color = 5128253
      ParentBackground = False
      TabOrder = 2
      Visible = False
    end
    object cxTextEdit1: TcxTextEdit
      Left = 81
      Top = 44
      Style.HotTrack = False
      TabOrder = 0
      Text = 'NewTable'
      Width = 121
    end
    object cxGrid1: TcxGrid
      Left = 18
      Top = 76
      Width = 516
      Height = 276
      TabOrder = 1
      object cxGTV1: TcxGridDBTableView
        Navigator.Buttons.CustomButtons = <>
        DataController.DataSource = DataSource1
        DataController.Summary.DefaultGroupSummaryItems = <>
        DataController.Summary.FooterSummaryItems = <>
        DataController.Summary.SummaryGroups = <>
        NewItemRow.SeparatorWidth = 2
        NewItemRow.Visible = True
        OptionsBehavior.GoToNextCellOnEnter = True
        OptionsData.Appending = True
        OptionsData.Inserting = False
        OptionsView.DataRowHeight = 25
        OptionsView.GroupByBox = False
        OptionsView.HeaderHeight = 25
        OptionsView.Indicator = True
        object cxGTV1FieldName: TcxGridDBColumn
          Caption = #21015#21517
          DataBinding.FieldName = 'FieldName'
          HeaderAlignmentHorz = taCenter
          Width = 161
        end
        object cxGTV1FieldType: TcxGridDBColumn
          Caption = #25968#25454#31867#22411
          DataBinding.FieldName = 'FieldType'
          HeaderAlignmentHorz = taCenter
          Width = 147
        end
        object cxGTV1FIsNull: TcxGridDBColumn
          Caption = #20801#35768'Null'#20540
          DataBinding.FieldName = 'FIsNull'
          PropertiesClassName = 'TcxCheckBoxProperties'
          Properties.ImmediatePost = True
          Properties.NullStyle = nssUnchecked
          HeaderAlignmentHorz = taCenter
          Width = 74
        end
        object cxGTV1FKey: TcxGridDBColumn
          DataBinding.FieldName = 'FKey'
          Visible = False
          Width = 55
        end
      end
      object cxGrid1Level1: TcxGridLevel
        GridView = cxGTV1
      end
    end
    object dxLayoutControl1Group_Root: TdxLayoutGroup
      AlignHorz = ahClient
      AlignVert = avClient
      ButtonOptions.Buttons = <>
      Hidden = True
      LayoutDirection = ldTabbed
      ShowBorder = False
      Index = -1
    end
    object dxLayoutItem3: TdxLayoutItem
      Parent = dxLayoutControl1Group_Root
      AlignVert = avClient
      CaptionOptions.ImageIndex = 12
      CaptionOptions.Text = 'SQL'#33050#26412
      CaptionOptions.Visible = False
      Control = Panel1
      ControlOptions.AutoColor = True
      ControlOptions.ShowBorder = False
      Index = 1
    end
    object dxLayoutGroup1: TdxLayoutGroup
      Parent = dxLayoutControl1Group_Root
      AlignHorz = ahClient
      AlignVert = avClient
      CaptionOptions.ImageIndex = 16
      CaptionOptions.Text = #34920#35774#35745
      ButtonOptions.Buttons = <>
      ItemIndex = 1
      Index = 0
    end
    object dxLayoutItem2: TdxLayoutItem
      Parent = dxLayoutGroup1
      CaptionOptions.ImageIndex = 16
      CaptionOptions.Text = #34920#21517#65306
      Control = cxTextEdit1
      ControlOptions.ShowBorder = False
      Index = 0
    end
    object dxLayoutItem1: TdxLayoutItem
      Parent = dxLayoutGroup1
      AlignHorz = ahClient
      AlignVert = avClient
      Control = cxGrid1
      ControlOptions.ShowBorder = False
      Index = 1
    end
  end
  object FDMemTable1: TFDMemTable
    Active = True
    FieldDefs = <
      item
        Name = 'FieldName'
        DataType = ftString
        Size = 50
      end
      item
        Name = 'FieldType'
        DataType = ftString
        Size = 50
      end
      item
        Name = 'FIsNull'
        DataType = ftBoolean
      end
      item
        Name = 'FKey'
        DataType = ftBoolean
      end>
    IndexDefs = <>
    FetchOptions.AssignedValues = [evMode]
    FetchOptions.Mode = fmAll
    ResourceOptions.AssignedValues = [rvSilentMode]
    ResourceOptions.SilentMode = True
    UpdateOptions.AssignedValues = [uvCheckRequired, uvAutoCommitUpdates]
    UpdateOptions.CheckRequired = False
    UpdateOptions.AutoCommitUpdates = True
    StoreDefs = True
    Left = 125
    Top = 151
  end
  object DataSource1: TDataSource
    DataSet = FDMemTable1
    Left = 232
    Top = 152
  end
end
