unit UDM;

interface

uses
  System.SysUtils, System.Classes, System.ImageList, Vcl.ImgList, Vcl.Controls,
  cxGraphics, FireDAC.Stan.Intf, FireDAC.Stan.Option, FireDAC.Stan.Error,
  FireDAC.UI.Intf, FireDAC.Phys.Intf, FireDAC.Stan.Def, FireDAC.Stan.Pool,
  FireDAC.Stan.Async, FireDAC.Phys, FireDAC.VCLUI.Wait, Data.DB, cxPC,
  FireDAC.Comp.Client, FireDAC.Phys.MSAccDef, FireDAC.Stan.ExprFuncs,
  FireDAC.Phys.SQLiteDef, FireDAC.Phys.SQLite, FireDAC.Phys.ODBCBase,
  FireDAC.Phys.MSAcc, FireDAC.Stan.Param, FireDAC.DatS, FireDAC.DApt.Intf,
  FireDAC.DApt, dxSkinsCore, dxSkinBlue, dxSkinSharpPlus, dxSkinSilver,
  FireDAC.Comp.DataSet, cxClasses, cxLookAndFeels, dxSkinsForm,
  dxLayoutLookAndFeels, Vcl.Dialogs, dxSkinOffice2013White, dxSkinWhiteprint,
  FireDAC.Phys.MSSQL, FireDAC.Phys.MSSQLDef, FireDAC.Comp.ScriptCommands,
  FireDAC.Stan.Util, FireDAC.Comp.Script;


const    //有S的表示为
  con_DBs = 1;
  con_DB = 2;
  con_Tables = 3;
  con_Table = 4;
  con_Views = 5;
  con_View = 6;
  con_Fields = 7;
  con_Field = 8;
  con_Keys = 9;
  con_Key = 10;
type
//自己定义一个类的原因就是以后可能 不使用 FireDac 使用UniDac
  TPPConn = class(TFDCustomConnection)
  private
    function GetProvider: string;
  public
    constructor Create(AOwner: TComponent); override;
    //destructor Destroy; override;
    property Connected;

    property Provider: string read GetProvider;
    function ShowConnEdit: Boolean;

    procedure GetTables(AList: TStrings);
    procedure GetViews(AList: TStrings);
    procedure GetFields(ATableName: string; AList: TStrings);
    procedure GetKeys(ATableName: string; AList: TStrings);
    procedure GetFieldTypes(AList: TStrings);

    function OpenSql(ASql: string): TDataSet;
    function ExecSQL(const ASQL: String): LongInt; overload;
  end;

  TDM = class(TDataModule)
    cxIL16: TcxImageList;
    FDConnection1: TFDConnection;
    FDPhysMSAccessDriverLink1: TFDPhysMSAccessDriverLink;
    FDPhysSQLiteDriverLink1: TFDPhysSQLiteDriverLink;
    dxSkinController1: TdxSkinController;
    cxLookAndFeelController1: TcxLookAndFeelController;
    dxLayoutLookAndFeelList1: TdxLayoutLookAndFeelList;
    dxLayoutCxLookAndFeel1: TdxLayoutCxLookAndFeel;
    OpenDialog1: TOpenDialog;
    FDCommand1: TFDCommand;
    FDQuery1: TFDQuery;
    FDScript1: TFDScript;
    procedure DataModuleCreate(Sender: TObject);
  private

  public

  end;

var
  DM: TDM;
  ActiveConn: TPPConn; //对象树中的当前链接
  FTabC: TcxTabControl; //首页中的页签管理组件
implementation
uses
  UCommon, UFrmConnEditor;
{%CLASSGROUP 'Vcl.Controls.TControl'}

{$R *.dfm}

{ TPPConn }

constructor TPPConn.Create(AOwner: TComponent);
begin
  inherited Create(AOwner);
  LoginPrompt := False;
  //FConn.GetTableNames();
end;


function TPPConn.ExecSQL(const ASQL: String): LongInt;
begin
  with TFDScript.Create(nil) do
  begin
    Connection := Self;
    SQLScripts.Clear;
    SQLScriptFileName := '';
    ScriptOptions.Reset;

    SQLScripts.Add.SQL.Text := ASQL;
      // Run it
    try
      ValidateAll;
      ExecuteAll;
    finally
      Free;
    end;
  end;
end;

procedure TPPConn.GetFields(ATableName: string; AList: TStrings);
begin
  GetFieldNames('', '', ATableName, '', AList);
end;

procedure TPPConn.GetFieldTypes(AList: TStrings);
const
  Access = 'Counter;SmallInt;Int;DateTime;Bit;VarChar(50);Text(200);Image';
  Sqlite = '';
begin

end;

procedure TPPConn.GetKeys(ATableName: string; AList: TStrings);
begin
  GetKeyFieldNames('', '', ATableName, '', AList);
end;

function TPPConn.GetProvider: string;
begin
  Result := DriverName;
end;

procedure TPPConn.GetTables(AList: TStrings);
begin
  GetTableNames('', '', '', AList, [osMy], [tkTable], True);
end;

procedure TPPConn.GetViews(AList: TStrings);
begin
  GetTableNames('', '', '', AList, [osMy], [tkView], True);
end;

function TPPConn.OpenSql(ASql: string): TDataSet;
begin
  Result := TFDQuery.Create(nil);
  with TFDQuery(Result) do
  begin
    Connection := Self;
    Open(ASQL);
  end;
end;

function TPPConn.ShowConnEdit: Boolean;
begin
  with TFrmConnEditor.Create(Self) do
  begin
    ShowModal;
    Free;
  end;
end;

procedure TDM.DataModuleCreate(Sender: TObject);
begin
  //FDScript1
   {FDQuery1.FetchAll
  FDCommand1.Execute('', [], []);
  FDCommand1.FetchOptions}
  //FDCommand1.State

  // FDQuery1.Command.
end;


end.
