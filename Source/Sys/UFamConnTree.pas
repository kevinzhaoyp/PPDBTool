{
数据库链接树
}
unit UFamConnTree;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes,
  Vcl.Graphics, Vcl.Controls, Vcl.Forms, Vcl.Dialogs, cxGraphics, cxControls,
  cxLookAndFeels, cxLookAndFeelPainters, cxCustomData, cxStyles, cxTL,
  cxTLdxBarBuiltInMenu, cxInplaceContainer, System.Actions, Vcl.ActnList,
  cxClasses, dxBar, cxTextEdit, UDM, dxSkinsCore, dxSkinBlue, dxSkinSharpPlus,
  dxSkinSilver, dxSkinsdxBarPainter, Vcl.Menus, dxSkinOffice2013White,
  dxSkinWhiteprint;

const
  con_DBDisImage = 6;  //数据库断开图标
  con_DBConnImage = 3; //数据库链接图标
  //对应树的Index
  con_InTitle = 0;
  con_InType = 1;
  con_InTag = 2;
type
  TFamConnTree = class(TFrame)
    ActionList1: TActionList;
    cxTL1: TcxTreeList;
    ActDisConn: TAction;
    ActRefresh: TAction;
    ActFilter: TAction;
    ActNewConn: TAction;
    ActDelConn: TAction;
    cxTLCTitle: TcxTreeListColumn;
    cxTLCTag: TcxTreeListColumn;
    dxBarManager1: TdxBarManager;
    dxBarManager1Bar1: TdxBar;
    dxBarSubItem1: TdxBarSubItem;
    dxBarButton1: TdxBarButton;
    dxBarButton2: TdxBarButton;
    dxBarButton3: TdxBarButton;
    dxBarButton4: TdxBarButton;
    dxBarButton5: TdxBarButton;
    ActTableNew: TAction;
    ActNewSql: TAction;
    actOpenAccess: TAction;
    actOpenSqlite: TAction;
    actOpenDB: TAction;
    dxBarButton6: TdxBarButton;
    cxTLCType: TcxTreeListColumn;
    PopupMenu1: TPopupMenu;
    N1: TMenuItem;
    N2: TMenuItem;
    ActTableDis: TAction;
    ReName: TAction;
    ActDel: TAction;
    N3: TMenuItem;
    N4: TMenuItem;
    N5: TMenuItem;
    N6: TMenuItem;
    ActTableEdit: TAction;
    N7: TMenuItem;
    N8: TMenuItem;
    ActSelToSqlEdit: TAction;
    Select1: TMenuItem;
    procedure ActNewConnExecute(Sender: TObject);
    procedure cxTL1Expanding(Sender: TcxCustomTreeList; ANode: TcxTreeListNode;
      var Allow: Boolean);
    procedure actOpenAccessExecute(Sender: TObject);
    procedure ActNewSqlExecute(Sender: TObject);
    procedure ActTableEditExecute(Sender: TObject);
  private
    FConn: TPPConn;

    procedure SetNodeValue(ANode: TcxTreeListNode; ATitle: string; AType: Integer; ATag: string = '');
  public
    { Public declarations }
    constructor Create(AOwner: TComponent); override;
    destructor Destroy; override;

    procedure AddChildren(ANode: TcxTreeListNode);
  end;

implementation
uses
  UMods, UCommon, UFrmSetConn, UFrmTableEdit;
{$R *.dfm}

procedure TFamConnTree.ActNewConnExecute(Sender: TObject);
var
  aConnName: string;
begin
  if not TFrmSetConn.FrmShow(aConnName) then Exit;
  if Trim(aConnName) = '' then Exit;
  //
  if cxTL1.FindNodeByText(aConnName, cxTLCTitle) <> nil then
  begin
    ShowMessageFmt('连接名：[%s]已存在', [aConnName]);
    Exit;
  end;
  //
  FConn.ConnectionString := AppConfig.ReadString(IniSection, aConnName, '');
  FConn.Connected := True;

  with cxTL1.Add do
  begin
    Values[cxTLCTitle.ItemIndex] := aConnName;
    if FConn.Connected then
      ImageIndex := con_DBConnImage
    else
      ImageIndex := con_DBDisImage;
    AddChild;
  end;
end;

procedure TFamConnTree.actOpenAccessExecute(Sender: TObject);
var
  aFileName, aConnStr, aDBName: string;
begin
//打开Access数据库
  with TOpenDialog.Create(self) do
  begin
    if Execute then
      aFileName := FileName;
    Free;
  end;
  case TAction(Sender).Tag of
    1: aConnStr := 'DriverID=MSAcc;';
    2: aConnStr := 'DriverID=SQLite;';
  end;
  aConnStr := aConnStr + 'Database=' + aFileName;
  FConn.ConnectionString := aConnStr;
  FConn.Connected := True;
  aDBName := ExtractFileName(aFileName);
  //判断是否存在
  if cxTL1.FindNodeByText(aDBName, cxTLCTitle) <> nil then
  begin
    ShowMessageFmt('连接名：[%s]已存在', [aDBName]);
    Exit;
  end;
  //增加到树上。
  with cxTL1.Add do
  begin
    Values[con_InTitle] := aDBName;
    Values[con_InTag] := aConnStr;
    Values[con_InType] := con_DB;
    if FConn.Connected then
      ImageIndex := con_DBConnImage
    else
      ImageIndex := con_DBDisImage;
    AddChild;//增加一个空的子节点。
  end;
end;

procedure TFamConnTree.ActTableEditExecute(Sender: TObject);
var
  aTableName: string;
begin //表操作
  if not Assigned(cxTL1.FocusedNode) then Exit;
  aTableName := cxTL1.FocusedNode.Values[con_InTitle];
  case TAction(Sender).Tag of
    1: DataEdit(aTableName); //编辑表数据
    2: NewSql('select * from ' + aTableName);//Select到当前查询窗口
  end;
end;

procedure TFamConnTree.AddChildren(ANode: TcxTreeListNode);
  procedure AddDBItems;
  begin
    SetNodeValue(ANode.AddChild, '表', con_Tables);
    SetNodeValue(ANode.AddChild, '视图', con_Views);
  end;
  procedure AddTableItems;
  begin
    SetNodeValue(ANode.AddChild, '列', con_Fields);
    SetNodeValue(ANode.AddChild, '键', con_Keys);
    //SetNodeValue(ANode.AddChild, '约束', con_Keys);
    //SetNodeValue(ANode.AddChild, '触发器', con_Keys);
    //SetNodeValue(ANode.AddChild, '索引', con_Keys);
    //SetNodeValue(ANode.AddChild, '统计信息', con_Keys);
  end;
  procedure AddItems(AType: Integer; AObjName: string = '');
  var
    aList: TStrings;
    i: Integer;
  begin
    aList := TStringList.Create;
    try
      if AType = con_Table then
        FConn.GetTables(aList)
      else if AType = con_View then
        FConn.GetViews(aList)
      else if AType = con_Field then
        FConn.GetFields(AObjName, aList)
      else if AType = con_Key then
        FConn.GetKeys(AObjName, aList);
      for i := 0 to aList.Count - 1 do
        SetNodeValue(ANode.AddChild, aList.Strings[i], AType);
    finally
      aList.Free;
    end;
  end;
begin
  ANode.DeleteChildren;
  case VarToInt(ANode.Values[con_InType]) of
    con_DB: AddDBItems;
    con_Tables: AddItems(con_Table);
    con_Views: AddItems(con_View);
    con_Table, con_View: AddTableItems; //
    con_Fields: AddItems(con_Field, ANode.Parent.Values[con_InTitle]);
    con_Keys: AddItems(con_Key, ANode.Parent.Values[con_InTitle]);
  end;
end;

constructor TFamConnTree.Create(AOwner: TComponent);
begin
  inherited Create(AOwner);
  FConn := TPPConn.Create(Self);
  ActiveConn := FConn;//以后加入Manage后可能 会使用。
end;

procedure TFamConnTree.cxTL1Expanding(Sender: TcxCustomTreeList;
  ANode: TcxTreeListNode; var Allow: Boolean);
begin
// 展开的时候加载下级对象
  AddChildren(ANode);
end;

destructor TFamConnTree.Destroy;
begin
  FreeAndNil(FConn);
  inherited;
end;

procedure TFamConnTree.ActNewSqlExecute(Sender: TObject);
begin
//新建Sql

end;

procedure TFamConnTree.SetNodeValue(ANode: TcxTreeListNode; ATitle: string;
  AType: Integer; ATag: string);
begin
  ANode.Values[con_InTitle] := ATitle;
  ANode.Values[con_InType] := AType;
  ANode.Values[con_InTag] := ATag;
  //
  ANode.ImageIndex := 31;
  //
  if AType in[con_Field, con_Key] then Exit;
  ANode.AddChildFirst;
end;

end.
