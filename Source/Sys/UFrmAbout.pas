unit UFrmAbout;

interface

uses Windows, SysUtils, Classes, Forms, Controls, StdCtrls, ExtCtrls, Graphics,
  jpeg;

type
  TFrmAbout = class(TForm)
    OKButton: TButton;
    Image1: TImage;
    ProductName: TLabel;
    Version: TLabel;
    Copyright: TLabel;
    Image2: TImage;
    LinkLabel1: TLinkLabel;
    procedure OKButtonClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    class procedure FrmShow;
  end;

var
  FrmAbout: TFrmAbout;

implementation

uses
  UCommon;
{$R *.dfm}

class procedure TFrmAbout.FrmShow;
  function GetBuildInfo: string; // ��ȡ�汾��
  var
    verinfosize: DWORD;
    verinfo: pointer;
    vervaluesize: DWORD;
    vervalue: pvsfixedfileinfo;
    dummy: DWORD;
    v1, v2, v3, v4: word;
  begin
    verinfosize := getfileversioninfosize(pchar(paramstr(0)), dummy);
    if verinfosize = 0 then
    begin
      dummy := getlasterror;
      result := '0.0.0.0';
    end;
    getmem(verinfo, verinfosize);
    getfileversioninfo(pchar(paramstr(0)), 0, verinfosize, verinfo);
    verqueryvalue(verinfo, '\', pointer(vervalue), vervaluesize);
    with vervalue^ do
    begin
      v1 := dwfileversionms shr 16;
      v2 := dwfileversionms and $FFFF;
      v3 := dwfileversionls shr 16;
      v4 := dwfileversionls and $FFFF;
    end;
    result := inttostr(v1) + '.' + inttostr(v2) + '.' + inttostr(v3) + '.' +
      inttostr(v4);
    freemem(verinfo, verinfosize);
  end;

begin
  with TFrmAbout.Create(Application) do
  begin
    ProductName.Caption := AppConfig.ReadString('Application', 'Title',
      Application.Title);
    Version.Caption := 'Version: ' + GetBuildInfo;
    if Sys.RegInfo < 1 then
      Copyright.Caption := 'δע��'
    else
      Copyright.Caption := '��ע��';
    ShowModal;
    Free;
  end;
end;

procedure TFrmAbout.OKButtonClick(Sender: TObject);
begin
  Self.ModalResult := mrOk;
end;

end.
