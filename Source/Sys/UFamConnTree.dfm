object FamConnTree: TFamConnTree
  Left = 0
  Top = 0
  Width = 260
  Height = 562
  TabOrder = 0
  object cxTL1: TcxTreeList
    Left = 0
    Top = 23
    Width = 260
    Height = 539
    Align = alClient
    Bands = <
      item
      end>
    Images = DM.cxIL16
    Navigator.Buttons.CustomButtons = <>
    OptionsSelection.CellSelect = False
    OptionsView.ColumnAutoWidth = True
    OptionsView.Headers = False
    OptionsView.Indicator = True
    PopupMenu = PopupMenu1
    TabOrder = 0
    OnExpanding = cxTL1Expanding
    object cxTLCTitle: TcxTreeListColumn
      Caption.Text = #23545#35937#21517#31216
      DataBinding.ValueType = 'String'
      Width = 145
      Position.ColIndex = 0
      Position.RowIndex = 0
      Position.BandIndex = 0
      Summary.FooterSummaryItems = <>
      Summary.GroupFooterSummaryItems = <>
    end
    object cxTLCType: TcxTreeListColumn
      Visible = False
      DataBinding.ValueType = 'String'
      Position.ColIndex = 2
      Position.RowIndex = 0
      Position.BandIndex = 0
      Summary.FooterSummaryItems = <>
      Summary.GroupFooterSummaryItems = <>
    end
    object cxTLCTag: TcxTreeListColumn
      Visible = False
      Caption.Text = #38142#25509#20018
      DataBinding.ValueType = 'String'
      Width = 111
      Position.ColIndex = 1
      Position.RowIndex = 0
      Position.BandIndex = 0
      Summary.FooterSummaryItems = <>
      Summary.GroupFooterSummaryItems = <>
    end
  end
  object ActionList1: TActionList
    Images = DM.cxIL16
    Left = 232
    Top = 136
    object ActTableEdit: TAction
      Tag = 1
      Category = 'Table'
      Caption = #32534#36753#34920
      OnExecute = ActTableEditExecute
    end
    object ActDisConn: TAction
      Caption = #26029#24320
      Hint = #26029#24320#24182#31227#38500
      ImageIndex = 6
    end
    object ActRefresh: TAction
      Caption = #21047#26032
      ImageIndex = 17
    end
    object ActFilter: TAction
      Caption = #36807#28388
      ImageIndex = 8
    end
    object ActNewConn: TAction
      Caption = #26032#24314#36830#25509
      ImageIndex = 15
      OnExecute = ActNewConnExecute
    end
    object ActDelConn: TAction
      Caption = #21024#38500#24403#21069#36830#25509
      ImageIndex = 14
    end
    object ActSelToSqlEdit: TAction
      Tag = 2
      Category = 'Table'
      Caption = 'Select'#21040#24403#21069#26597#35810#31383#21475
      OnExecute = ActTableEditExecute
    end
    object ActTableNew: TAction
      Category = 'Table'
      Caption = #26032#24314#34920
    end
    object ActNewSql: TAction
      Category = 'Common'
      Caption = #26032#24314#26597#35810
      OnExecute = ActNewSqlExecute
    end
    object actOpenAccess: TAction
      Tag = 1
      Caption = #25171#24320'Access'#25968#25454#24211
      ImageIndex = 0
      OnExecute = actOpenAccessExecute
    end
    object actOpenSqlite: TAction
      Tag = 2
      Caption = #25171#24320'Sqlite'#25968#25454
      ImageIndex = 0
    end
    object actOpenDB: TAction
      Caption = #25171#24320#25968#25454#24211
      ImageIndex = 0
    end
    object ActTableDis: TAction
      Category = 'Table'
      Caption = #35774#35745#34920
    end
    object ReName: TAction
      Category = 'Common'
      Caption = #37325#21629#21517
    end
    object ActDel: TAction
      Category = 'Common'
      Caption = #21024#38500
    end
  end
  object dxBarManager1: TdxBarManager
    AlwaysMerge = True
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'Microsoft YaHei UI'
    Font.Style = []
    Categories.Strings = (
      #40664#35748)
    Categories.ItemsVisibles = (
      2)
    Categories.Visibles = (
      True)
    ImageOptions.Images = DM.cxIL16
    PopupMenuLinks = <>
    UseSystemFont = True
    Left = 232
    Top = 208
    DockControlHeights = (
      0
      0
      23
      0)
    object dxBarManager1Bar1: TdxBar
      AllowClose = False
      AllowCustomizing = False
      AllowQuickCustomizing = False
      AllowReset = False
      BorderStyle = bbsNone
      Caption = #24037#20855#26639
      CaptionButtons = <>
      DockedDockingStyle = dsTop
      DockedLeft = 0
      DockedTop = 0
      DockingStyle = dsTop
      FloatLeft = 270
      FloatTop = 0
      FloatClientWidth = 0
      FloatClientHeight = 0
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxBarSubItem1'
        end
        item
          Visible = True
          ItemName = 'dxBarButton5'
        end
        item
          Visible = True
          ItemName = 'dxBarButton2'
        end
        item
          Visible = True
          ItemName = 'dxBarButton3'
        end>
      OneOnRow = True
      Row = 0
      UseOwnFont = False
      UseRestSpace = True
      Visible = True
      WholeRow = True
    end
    object dxBarSubItem1: TdxBarSubItem
      Caption = #25968#25454#24211
      Category = 0
      Visible = ivAlways
      ImageIndex = 2
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxBarButton1'
        end
        item
          Visible = True
          ItemName = 'dxBarButton4'
        end>
    end
    object dxBarButton1: TdxBarButton
      Action = actOpenAccess
      Category = 0
    end
    object dxBarButton2: TdxBarButton
      Action = ActFilter
      Category = 0
    end
    object dxBarButton3: TdxBarButton
      Action = ActRefresh
      Category = 0
    end
    object dxBarButton4: TdxBarButton
      Action = actOpenSqlite
      Category = 0
    end
    object dxBarButton5: TdxBarButton
      Action = ActDisConn
      Category = 0
    end
    object dxBarButton6: TdxBarButton
      Action = actOpenDB
      Category = 0
    end
  end
  object PopupMenu1: TPopupMenu
    Left = 232
    Top = 288
    object N1: TMenuItem
      Action = ActNewSql
    end
    object N2: TMenuItem
      Action = ActTableNew
    end
    object N3: TMenuItem
      Action = ActTableDis
    end
    object N7: TMenuItem
      Action = ActTableEdit
    end
    object N8: TMenuItem
      Caption = #29983#25104#33050#26412
      object Select1: TMenuItem
        Action = ActSelToSqlEdit
      end
    end
    object N6: TMenuItem
      Caption = '-'
    end
    object N4: TMenuItem
      Action = ActDel
    end
    object N5: TMenuItem
      Action = ReName
    end
  end
end
