{
数据库链接设置
与PPDAL一起使用。
ini文件
删除/修改 还没做。

[DBconn] //节名
MainConn=SqlServer  当前主数据库链接
SqlServer=........
...
}
unit UFrmSetConn;

interface

uses
  System.Classes, System.SysUtils,
  Vcl.Controls, Vcl.Forms,
  cxButtons, cxVGrid,
  cxPC, cxGraphics, cxControls, cxLookAndFeels, cxLookAndFeelPainters,
  dxBarBuiltInMenu, cxStyles, cxEdit, cxTextEdit, cxDropDownEdit, cxSpinEdit,
  Vcl.Menus, Vcl.ImgList, Vcl.StdCtrls, cxInplaceContainer,
  Vcl.Grids, Vcl.ValEdit, Vcl.ExtCtrls, cxButtonEdit, Vcl.ActnList,
  System.Actions, System.ImageList, Vcl.Dialogs, dxSkinsCore, dxSkinBlue,
  dxSkinSharpPlus, dxSkinSilver, dxSkinscxPCPainter, dxSkinOffice2013White,
  dxSkinWhiteprint;

const
  IniSection = 'DBconn';
type
  TFrmSetConn = class(TForm)
    cxPC1: TcxPageControl;
    cxTabSheet1: TcxTabSheet;
    cxVG1: TcxVerticalGrid;
    cxProvider: TcxEditorRow;
    cxButton1: TcxButton;
    cxButton2: TcxButton;
    cxButton3: TcxButton;
    imagelist: TImageList;
    cxVGCR1: TcxCategoryRow;
    pnl1: TPanel;
    actl1: TActionList;
    act1: TAction;
    cxConnName: TcxMultiEditorRow;
    Database: TcxEditorRow;
    OpenDialog1: TOpenDialog;
    procedure cxButton1Click(Sender: TObject);
    procedure cxButton2Click(Sender: TObject);
    procedure cxProviderEditPropertiesEditValueChanged(Sender: TObject);
    procedure cxConnNameEditors0EditPropertiesEditValueChanged(Sender: TObject);
    procedure cxConnNameEditors1EditPropertiesButtonClick(Sender: TObject;
      AButtonIndex: Integer);
    procedure DatabaseEditPropertiesButtonClick(Sender: TObject;
      AButtonIndex: Integer);
  private
    procedure LoadConns;//加载Ini中的所有Conn
    procedure LoadConnStr(AConnName: string);//加载选择的链接信息
    procedure LoadProperties(AFactDrive: string);//加载更多属性设置
    //
    function GetConnStr(AEncrypt: Boolean = False): string;//获取当前连接信息
  public
    class function FrmShow(var AConnStr: string): Boolean;
  end;

implementation
uses
  UDM, UCommon;
{$R *.dfm}

{ TFrmSetConn }

procedure TFrmSetConn.cxButton1Click(Sender: TObject);
begin
  //测试连接

end;

procedure TFrmSetConn.cxButton2Click(Sender: TObject);
var
  aConnName: string;
begin
  //保存到Ini中
  //aConnName := VarToStr(cxConnName.Properties.Value);
  aConnName := VarToStr(cxConnName.Properties.Editors[0].Value);
  AppConfig.WriteString(IniSection, aConnName, GetConnStr(True));
  Self.ModalResult := mrOk;
end;

procedure TFrmSetConn.cxConnNameEditors0EditPropertiesEditValueChanged(
  Sender: TObject);
begin
  //
  LoadConnStr(VarToStr(cxConnName.Properties.Editors[0].Value));
end;

procedure TFrmSetConn.cxConnNameEditors1EditPropertiesButtonClick(
  Sender: TObject; AButtonIndex: Integer);
var
  aStr: string;
  aIndex: Integer;
begin
  //删除当前链接
  aStr := VarToStr(cxConnName.Properties.Editors[0].Value);
  AppConfig.DeleteKey(IniSection, aStr);
  cxConnName.Properties.Editors[0].Value := '';
  with TcxComboBoxProperties(cxConnName.Properties.Editors[0].EditProperties) do
  begin
    aIndex := Items.IndexOf(aStr);
    if aIndex < 0 then Exit;
    Items.Delete(aIndex);
    if Items.Count <= 0 then Exit;
    cxConnName.Properties.Editors[0].Value := Items.Strings[0];
    LoadConnStr(Items.Strings[0]);
  end;
end;

procedure TFrmSetConn.cxProviderEditPropertiesEditValueChanged(Sender: TObject);
begin
  LoadProperties(VarToStr(cxProvider.Properties.Value));
end;

procedure TFrmSetConn.DatabaseEditPropertiesButtonClick(Sender: TObject;
  AButtonIndex: Integer);
begin
  if OpenDialog1.Execute then
    Database.Properties.Value := OpenDialog1.FileName;
end;

class function TFrmSetConn.FrmShow(var AConnStr: string): Boolean;
begin
  with TFrmSetConn.Create(Application) do
  begin
    LoadConns; //从配制文件中读取已保存的信息
    if Trim(AConnStr) <> '' then
      cxConnName.Properties.Editors[0].Value := AConnStr;

    Result := ShowModal = mrOk;
    AConnStr := VarToStr(cxConnName.Properties.Editors[0].Value);
    Free;
  end;
end;

function TFrmSetConn.GetConnStr(AEncrypt: Boolean): string;
var
  i: Integer;
  aStr: string;
begin
{ AEncrypt 要传进来是因为，保存到Ini中是要加密的，传给Dll是不用加密的。
Dll不负责加密解密功能，是因为每个系统 可能采用的加密技术不一样。
在这可以做整个加密 或者 密码 加密 }
  Result := 'Provider=' + VarToStr(cxProvider.Properties.Value);
  for i := 0 to cxVGCR1.Count - 1 do
  begin
    if cxVGCR1.Rows[i].Visible then
    begin
      aStr := TcxEditorRow(cxVGCR1.Rows[i]).Name;
      Result := Result + Format(';%s=%s', [aStr,
        VarToStr(TcxEditorRow(cxVGCR1.Rows[i]).Properties.Value)]);
    end;
  end;
end;

procedure TFrmSetConn.LoadConnStr(AConnName: string);
var
  aList: TStrings;
  aStr: string;
  i: Integer;
begin
  //在这可以做整个解密 或者 密码 解密
  aStr := AppConfig.ReadString(IniSection, AConnName, '');
  //aConnStr := DecStr(aConnStr);
  aList := SplitStr(aStr);
  cxVG1.BeginUpdate;
  try
    cxProvider.Properties.Value := aList.Values['Provider'];
    LoadProperties(aList.Values['Provider']);
    //初始化显示的属性
    for i := 0 to cxVGCR1.Count - 1 do
    begin
      if cxVGCR1.Rows[i].Visible then
      begin
        aStr := TcxEditorRow(cxVGCR1.Rows[i]).Name;
        TcxEditorRow(cxVGCR1.Rows[i]).Properties.Value := aList.Values[aStr];
      end;
    end;
  finally
    aList.Free;
    cxVG1.EndUpdate;
  end;
end;

procedure TFrmSetConn.LoadProperties(AFactDrive: string);
var
  aStr: string;
  aRow: TcxCustomRow;
  aList: TStrings;
  i: Integer;
begin
  if Trim(AFactDrive) = '' then Exit;//错误的链接串
  //加载并显示
  cxVG1.BeginUpdate;
  aList := SplitStr(aStr);
  try
    //
    {for i := 0 to aList.Count - 1 do
    begin
      aRow := cxVG1.AddChild(cxVGCR1, TcxEditorRow);
      aRow.Name := aList.Names[i];
      if Pos('Password', aRow.Name) > 0 then
        GetEditProperties(TcxEditorRow(aRow).Properties, XPassWord);
      TcxEditorRow(aRow).Properties.Caption := aList.ValueFromIndex[i];
    end; }
    Database.Properties.Value := aList.Values['Database'];
  finally
    aList.Free;
    cxVG1.EndUpdate;
  end;
end;

procedure TFrmSetConn.LoadConns;
begin
  cxVG1.BeginUpdate;
  try
    //从Ini中读取已设置的列表
    with TcxComboBoxProperties(cxConnName.Properties.Editors[0].EditProperties) do
    begin
      Items.Clear;
      AppConfig.ReadSection(IniSection, Items);
      if Items.IndexOf('MainConn') > -1 then
        Items.Delete(Items.IndexOf('MainConn'));//删除 MainConn
    end;
  finally
    cxVG1.EndUpdate;
  end;
  //加载当前链接
  LoadConnStr(VarToStr(cxConnName.Properties.Editors[0].Value));
end;

end.
