{
���ݿ���������

}
unit UFrmConnEditor;

interface

uses
  System.Classes, System.SysUtils,
  Vcl.Controls, Vcl.Forms, UDM,
  cxButtons, cxVGrid,
  cxPC, cxGraphics, cxControls, cxLookAndFeels, cxLookAndFeelPainters,
  dxBarBuiltInMenu, cxStyles, cxEdit, cxTextEdit, cxDropDownEdit, cxSpinEdit,
  Vcl.Menus, Vcl.ImgList, Vcl.StdCtrls, cxInplaceContainer,
  Vcl.Grids, Vcl.ValEdit, Vcl.ExtCtrls, cxButtonEdit, Vcl.ActnList,
  System.Actions, System.ImageList, Vcl.Dialogs, dxSkinsCore, dxSkinBlue,
  dxSkinSharpPlus, dxSkinSilver, dxSkinscxPCPainter, cxImageComboBox,
  dxSkinOffice2013White, dxSkinWhiteprint;

type
  TFrmConnEditor = class(TForm)
    cxVG1: TcxVerticalGrid;
    cxProvider: TcxEditorRow;
    cxButton2: TcxButton;
    cxButton3: TcxButton;
    pnl1: TPanel;
    actl1: TActionList;
    actCreate: TAction;
    Database: TcxEditorRow;
    cxERVersion: TcxEditorRow;
    FileSaveDialog1: TFileSaveDialog;
    actSaveFile: TAction;
    cxERAddress: TcxEditorRow;
    cxERPort: TcxEditorRow;
    cxERUser: TcxEditorRow;
    cxERPassWord: TcxEditorRow;
    procedure cxButton2Click(Sender: TObject);
    procedure actSaveFileExecute(Sender: TObject);
    procedure actCreateExecute(Sender: TObject);
  private
    FConn: TPPConn;
  public
    constructor Create(AConn: TPPConn); overload;
  end;

implementation
uses
  UCommon;
{$R *.dfm}

{ TFrmSetConn }

procedure TFrmConnEditor.actCreateExecute(Sender: TObject);
var
  aProvider, aConnStr: string;
begin
  aProvider := cxProvider.Properties.Value;
  aConnStr := Format('DriverID=%s;', [aProvider]);
  aConnStr := aConnStr + 'Database=' +
    Database.Properties.Value;
  FConn.ConnectionString := aConnStr;

  FConn.Connected := True;
  Self.ModalResult := mrOk;
end;

procedure TFrmConnEditor.actSaveFileExecute(Sender: TObject);
begin
  if FileSaveDialog1.Execute then
    Database.Properties.Value := FileSaveDialog1.FileName;
end;

constructor TFrmConnEditor.Create(AConn: TPPConn);
begin
  inherited Create(AConn);
  FConn := AConn;
end;

procedure TFrmConnEditor.cxButton2Click(Sender: TObject);
begin
  Self.ModalResult := mrOk;
end;



end.
