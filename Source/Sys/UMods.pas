{

功能调用中心
如果能实现 使用注册的方式来统一调用。可以主页面放一起。
}
unit UMods;

interface

uses
  Windows, Forms, Classes, ActnList, SysUtils, System.Variants,
  UDM, cxPC;
  { Action调用，APar为附加参数多个可用;分割  调用成功返回True，否则返回False
    支持 TRzPageControl 页显示 }
var
  MaxNewSql: Integer; //记录新建Sql窗体的个数，只记录最大的

  procedure NewSql(ASql: string);//新建Sql
  procedure DataEdit(ATableName: string; ARows: Integer = 0);//表数据编辑 ARows=0表示所有行
  procedure EditTable(ATableName: string);
implementation

uses
  UFrmBase, UFrmSqlEdit, UFrmTableEdit;

procedure NewSql(ASql: string);
var
  aTitle: string;
  aFrm: TForm;
  aIndex: Integer;
begin
  Assert(FTabC <> nil);
  aTitle := 'NewSql' + IntToStr(MaxNewSql);
  inc(MaxNewSql); //
  FTabC.Tabs.BeginUpdate;
  try
    aFrm := TFrmSqlEdit.Create(Application);
    aFrm.Caption := aTitle;
    aIndex := FTabC.Tabs.AddObject(aTitle, aFrm);
    FTabC.TabIndex := aIndex;
    aFrm.Parent := FTabC;
    TFrmSqlEdit(aFrm).NewSql(ASql);
    aFrm.Show;
  finally
    FTabC.Tabs.EndUpdate;
  end;
end;

procedure DataEdit(ATableName: string; ARows: Integer);
var
  aTop, aTitle: string;
  aFrm: TForm;
  aIndex: Integer;
begin
  Assert(FTabC <> nil);
  aTitle := Format('表 %s 数据编辑', [ATableName]);
  if ARows <= 0 then
    aTop := '*'
  else
    aTop := Format('%d *', [ARows]);
  FTabC.Tabs.BeginUpdate;
  try
    aFrm := TFrmSqlEdit.Create(Application);
    aFrm.Caption := aTitle;
    aIndex := FTabC.Tabs.AddObject(aTitle, aFrm);
    FTabC.TabIndex := aIndex;
    aFrm.Parent := FTabC;
    TFrmSqlEdit(aFrm).NewSql(Format('select %s from %s', [aTop, ATableName]));
    TFrmSqlEdit(aFrm).FrmStyle := fsDataEdit;
    TFrmSqlEdit(aFrm).ActOpen.Execute;
    aFrm.Show;
  finally
    FTabC.Tabs.EndUpdate;
  end;
end;

procedure EditTable(ATableName: string);
var
  aFrm: TForm;
  aIndex: Integer;
begin
  Assert(FTabC <> nil);
  FTabC.Tabs.BeginUpdate;
  try
    aFrm := TFrmTableEdit.FrmShow(ActiveConn, ATableName);
    aIndex := FTabC.Tabs.AddObject('新表1', aFrm);
    FTabC.TabIndex := aIndex;
    aFrm.Parent := FTabC;
    aFrm.Show;
  finally
    FTabC.Tabs.EndUpdate;
  end;
end;

{ 自动处理 }
initialization
  MaxNewSql := 0;
finalization

end.
