object FrmMain: TFrmMain
  Left = 0
  Top = 0
  Caption = 'PP'#25968#25454#24211#31649#29702#24037#20855
  ClientHeight = 509
  ClientWidth = 799
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  FormStyle = fsMDIForm
  OldCreateOrder = True
  Position = poScreenCenter
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object dxDockSite1: TdxDockSite
    Left = 0
    Top = 73
    Width = 799
    Height = 416
    AllowDockClients = [dtClient, dtLeft, dtRight, dtBottom]
    Align = alClient
    ExplicitWidth = 764
    ExplicitHeight = 413
    DockingType = 5
    OriginalWidth = 799
    OriginalHeight = 416
    object dxLayoutDockSite3: TdxLayoutDockSite
      Left = 200
      Top = 0
      Width = 599
      Height = 416
      ParentShowHint = False
      ShowHint = True
      ExplicitWidth = 564
      ExplicitHeight = 413
      DockingType = 0
      OriginalWidth = 300
      OriginalHeight = 200
      object dxLayoutDockSite2: TdxLayoutDockSite
        Left = 0
        Top = 0
        Width = 599
        Height = 416
        DockingType = 0
        OriginalWidth = 300
        OriginalHeight = 200
      end
      object dxDPClient: TdxDockPanel
        Left = 0
        Top = 0
        Width = 599
        Height = 416
        AllowDock = [dtClient]
        AllowDockClients = []
        AllowFloating = True
        AutoHide = False
        Caption = 'dxDPClient'
        CustomCaptionButtons.Buttons = <>
        ShowCaption = False
        TabsProperties.CustomButtons.Buttons = <>
        DockingType = 0
        OriginalWidth = 185
        OriginalHeight = 140
        object cxTCClient: TcxTabControl
          Left = 0
          Top = 0
          Width = 587
          Height = 407
          Align = alClient
          TabOrder = 0
          Properties.AllowTabDragDrop = True
          Properties.CloseButtonMode = cbmActiveTab
          Properties.CustomButtons.Buttons = <>
          Properties.HotTrack = True
          Properties.Options = [pcoAlwaysShowGoDialogButton, pcoCloseButton, pcoGoDialog, pcoGradient, pcoGradientClientArea, pcoRedrawOnResize]
          Properties.ShowButtonHints = True
          Properties.ShowTabHints = True
          Properties.TabIndex = 0
          Properties.Tabs.Strings = (
            '12314'
            '1234234')
          OnCanCloseEx = cxTCClientCanCloseEx
          OnChange = cxTCClientChange
          ExplicitWidth = 552
          ExplicitHeight = 404
          ClientRectBottom = 402
          ClientRectLeft = 5
          ClientRectRight = 582
          ClientRectTop = 27
        end
      end
    end
    object dxDPConnTree: TdxDockPanel
      Left = 0
      Top = 0
      Width = 200
      Height = 416
      AllowDock = [dtLeft, dtRight]
      AllowFloating = True
      AutoHide = False
      Caption = #23545#35937#36164#28304#31649#29702#22120
      CustomCaptionButtons.Buttons = <>
      CustomCaptionButtons.Images = DM.cxIL16
      ImageIndex = 26
      TabsProperties.CustomButtons.Buttons = <>
      TabsProperties.HotTrack = True
      ExplicitHeight = 413
      DockingType = 1
      OriginalWidth = 200
      OriginalHeight = 140
      inline FamConnTree1: TFamConnTree
        Left = 0
        Top = 0
        Width = 188
        Height = 378
        Align = alClient
        TabOrder = 0
        ExplicitWidth = 188
        ExplicitHeight = 375
        inherited cxTL1: TcxTreeList
          Width = 188
          Height = 355
          ExplicitTop = 23
          ExplicitWidth = 188
          ExplicitHeight = 352
        end
        inherited dxBarManager1: TdxBarManager
          DockControlHeights = (
            0
            0
            23
            0)
        end
      end
    end
  end
  object dxStatusBar1: TdxStatusBar
    Left = 0
    Top = 489
    Width = 799
    Height = 20
    Images = DM.cxIL16
    Panels = <
      item
        PanelStyleClassName = 'TdxStatusBarTextPanelStyle'
        Fixed = False
      end
      item
        PanelStyleClassName = 'TdxStatusBarKeyboardStatePanelStyle'
        PanelStyle.CapsLockKeyAppearance.ActiveCaption = 'CAPS'
        PanelStyle.CapsLockKeyAppearance.InactiveCaption = 'CAPS'
        PanelStyle.NumLockKeyAppearance.ActiveCaption = 'NUM'
        PanelStyle.NumLockKeyAppearance.InactiveCaption = 'NUM'
        PanelStyle.ScrollLockKeyAppearance.ActiveCaption = 'SCRL'
        PanelStyle.ScrollLockKeyAppearance.InactiveCaption = 'SCRL'
        PanelStyle.InsertKeyAppearance.ActiveCaption = 'OVR'
        PanelStyle.InsertKeyAppearance.InactiveCaption = 'INS'
      end>
    PaintStyle = stpsUseLookAndFeel
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = []
    ExplicitTop = 486
    ExplicitWidth = 764
  end
  object dxDockingManager1: TdxDockingManager
    Color = clDefault
    DefaultLayoutSiteProperties.ShowHint = True
    DefaultHorizContainerSiteProperties.CustomCaptionButtons.Buttons = <>
    DefaultHorizContainerSiteProperties.Dockable = True
    DefaultHorizContainerSiteProperties.ImageIndex = -1
    DefaultVertContainerSiteProperties.CustomCaptionButtons.Buttons = <>
    DefaultVertContainerSiteProperties.Dockable = True
    DefaultVertContainerSiteProperties.ImageIndex = -1
    DefaultVertContainerSiteProperties.ShowCaption = False
    DefaultTabContainerSiteProperties.CustomCaptionButtons.Buttons = <>
    DefaultTabContainerSiteProperties.Dockable = True
    DefaultTabContainerSiteProperties.ImageIndex = -1
    DefaultTabContainerSiteProperties.ShowCaption = False
    DefaultTabContainerSiteProperties.TabsProperties.CloseButtonMode = cbmActiveTab
    DefaultTabContainerSiteProperties.TabsProperties.CustomButtons.Buttons = <>
    DefaultTabContainerSiteProperties.TabsProperties.HotTrack = True
    DefaultTabContainerSiteProperties.TabsProperties.Options = [pcoAlwaysShowGoDialogButton, pcoCloseButton, pcoGoDialog, pcoGradient, pcoGradientClientArea, pcoNoArrows, pcoRedrawOnResize]
    DefaultTabContainerSiteProperties.TabsProperties.TabPosition = tpTop
    DockStyle = dsVS2005
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = []
    Images = DM.cxIL16
    Left = 392
    Top = 112
    PixelsPerInch = 96
  end
  object dxBar1: TdxBarManager
    AlwaysMerge = True
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'Microsoft YaHei UI'
    Font.Style = []
    Categories.Strings = (
      'File'
      'View'
      'Style'
      'Help'
      'Menus'
      'Popup')
    Categories.ItemsVisibles = (
      2
      2
      2
      2
      2
      2)
    Categories.Visibles = (
      True
      True
      True
      True
      True
      True)
    ImageOptions.StretchGlyphs = False
    PopupMenuLinks = <>
    UseSystemFont = True
    Left = 278
    Top = 112
    DockControlHeights = (
      0
      0
      73
      0)
    object BarManagerBar1: TdxBar
      Caption = 'Main Menu'
      CaptionButtons = <>
      DockedDockingStyle = dsTop
      DockedLeft = 0
      DockedTop = 0
      DockingStyle = dsTop
      FloatLeft = 43
      FloatTop = 96
      FloatClientWidth = 132
      FloatClientHeight = 38
      IsMainMenu = True
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxBarSubItemFile'
        end
        item
          Visible = True
          ItemName = 'dxBarSubItemInsert'
        end
        item
          Visible = True
          ItemName = 'dxBarLIDockStyle'
        end
        item
          Visible = True
          ItemName = 'siStyles'
        end
        item
          Visible = True
          ItemName = 'siHelp'
        end>
      MultiLine = True
      OneOnRow = True
      Row = 0
      UseOwnFont = False
      Visible = True
      WholeRow = True
    end
    object BarManagerBar2: TdxBar
      Caption = 'File'
      CaptionButtons = <>
      DockedDockingStyle = dsTop
      DockedLeft = 0
      DockedTop = 26
      DockingStyle = dsTop
      FloatLeft = 404
      FloatTop = 341
      FloatClientWidth = 46
      FloatClientHeight = 22
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxBarButtonLoad'
        end
        item
          Visible = True
          ItemName = 'dxBarButtonSave'
        end>
      OldName = 'File'
      OneOnRow = True
      Row = 1
      UseOwnFont = False
      Visible = True
      WholeRow = False
    end
    object dxBarButtonLoad: TdxBarLargeButton
      Caption = #26032#24314'Sql'
      Category = 0
      Hint = #26032#24314'Sql'
      Visible = ivAlways
      OnClick = dxBarButtonLoadClick
    end
    object dxBarButtonSave: TdxBarLargeButton
      Caption = #27979#35797'2'
      Category = 0
      Hint = #27979#35797'2'
      Visible = ivAlways
      OnClick = dxBarButtonSaveClick
    end
    object dxBarButtonExit: TdxBarLargeButton
      Action = actExit
      Category = 0
      HotImageIndex = 38
    end
    object siStyles: TdxBarSubItem
      Caption = 'Styles'
      Category = 0
      Visible = ivAlways
      ItemLinks = <>
    end
    object dxBarLIDockStyle: TdxBarListItem
      Caption = 'Dock Style'
      Category = 0
      Visible = ivAlways
      OnClick = dxBarLIDockStyleClick
      ItemIndex = 1
      Items.Strings = (
        'Standard'
        'VS2005')
      ShowCheck = True
      ShowNumbers = False
    end
    object dxBarButtonStartPage: TdxBarLargeButton
      Action = actAbout
      Category = 1
    end
    object dxBarButtonObjectExplorer: TdxBarLargeButton
      Caption = #23545#35937#36164#28304#31649#29702#22120
      Category = 1
      Hint = #23545#35937#36164#28304#31649#29702#22120
      Visible = ivAlways
      LargeImageIndex = 32
      OnClick = dxBarButtonObjectExplorerClick
    end
    object dxBarButtonWatch: TdxBarLargeButton
      Caption = 'Watch'
      Category = 1
      Hint = 'Watch'
      Visible = ivAlways
      LargeImageIndex = 33
    end
    object dxBarButtonOutput: TdxBarLargeButton
      Caption = 'Output'
      Category = 1
      Hint = 'Output'
      Visible = ivAlways
      LargeImageIndex = 29
    end
    object dxBarButtonCallStack: TdxBarLargeButton
      Caption = 'CallStack'
      Category = 1
      Hint = 'CallStack'
      Visible = ivAlways
      LargeImageIndex = 27
    end
    object dxBarButtonProperties: TdxBarLargeButton
      Caption = 'Properties'
      Category = 1
      Hint = 'Properties'
      Visible = ivAlways
      LargeImageIndex = 30
    end
    object dxBarButtonClassView: TdxBarLargeButton
      Caption = 'ClassView'
      Category = 1
      Hint = 'ClassView'
      Visible = ivAlways
      LargeImageIndex = 28
    end
    object dxBarButtonSolutionExplorer: TdxBarLargeButton
      Caption = 'SolutionExplorer'
      Category = 1
      Hint = 'SolutionExplorer'
      Visible = ivAlways
      LargeImageIndex = 31
    end
    object dxBarSubItemOtherWindows: TdxBarSubItem
      Caption = 'Other Windows'
      Category = 1
      Visible = ivAlways
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxBarButtonCallStack'
        end
        item
          Visible = True
          ItemName = 'dxBarButtonOutput'
        end
        item
          Visible = True
          ItemName = 'dxBarButtonWatch'
        end>
    end
    object dxBarButton1: TdxBarLargeButton
      Category = 3
      Visible = ivAlways
      HotImageIndex = 39
    end
    object dxBarButton2: TdxBarLargeButton
      Category = 3
      Visible = ivAlways
    end
    object dxBarButton3: TdxBarLargeButton
      Category = 3
      Visible = ivAlways
    end
    object dxBarButton4: TdxBarLargeButton
      Category = 3
      Visible = ivAlways
    end
    object dxBarButton5: TdxBarLargeButton
      Category = 3
      Visible = ivAlways
    end
    object dxBarButton6: TdxBarLargeButton
      Category = 3
      Visible = ivAlways
    end
    object dxBarButton7: TdxBarLargeButton
      Category = 3
      Visible = ivAlways
    end
    object dxBarLargeButton1: TdxBarLargeButton
      Category = 3
      Visible = ivAlways
    end
    object dxBarLargeButton2: TdxBarLargeButton
      Category = 3
      Visible = ivAlways
    end
    object dxBarSubItemFile: TdxBarSubItem
      Caption = #25991#20214'(&F)'
      Category = 4
      Visible = ivAlways
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxBarButtonExit'
        end>
    end
    object dxBarSubItemInsert: TdxBarSubItem
      Caption = #35270#22270'(&V)'
      Category = 4
      Visible = ivAlways
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxBarButtonObjectExplorer'
        end>
    end
    object dxBarSubItemWindow: TdxBarSubItem
      Caption = '&Window'
      Category = 4
      Visible = ivAlways
      ItemLinks = <>
    end
    object siHelp: TdxBarSubItem
      Caption = #24110#21161'(&H)'
      Category = 4
      Visible = ivAlways
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxBarButtonStartPage'
        end>
    end
    object dxBarButtonDockable: TdxBarButton
      Caption = #21487#20572#38752'(&K)'
      Category = 5
      Hint = #21487#20572#38752'(K)'
      Visible = ivAlways
      ButtonStyle = bsChecked
    end
    object dxBarButtonHide: TdxBarButton
      Caption = #38544#34255'(&H)'
      Category = 5
      Hint = #38544#34255'(H)'
      Visible = ivAlways
    end
    object dxBarButtonFloating: TdxBarButton
      Caption = #28014#21160'(&F)'
      Category = 5
      Hint = #28014#21160'(F)'
      Visible = ivAlways
      ButtonStyle = bsChecked
    end
    object dxBarButtonAutoHide: TdxBarButton
      Caption = #33258#21160#38544#34255'(&A)'
      Category = 5
      Hint = #33258#21160#38544#34255'(A)'
      Visible = ivAlways
      ButtonStyle = bsChecked
    end
  end
  object ActionList1: TActionList
    Images = DM.cxIL16
    Left = 280
    Top = 201
    object Action1: TAction
      Caption = #27979#35797
    end
    object actExit: TAction
      Category = 'File'
      Caption = #36864#20986
      ShortCut = 16472
      OnExecute = actExitExecute
    end
    object actAbout: TAction
      Category = 'Help'
      Caption = #20851#20110
    end
  end
  object dxBPMContext: TdxBarPopupMenu
    BarManager = dxBar1
    ItemLinks = <
      item
        Visible = True
        ItemName = 'dxBarButtonDockable'
      end
      item
        Visible = True
        ItemName = 'dxBarButtonHide'
      end
      item
        Visible = True
        ItemName = 'dxBarButtonFloating'
      end
      item
        Visible = True
        ItemName = 'dxBarButtonAutoHide'
      end>
    UseOwnFont = False
    Left = 392
    Top = 200
  end
end
