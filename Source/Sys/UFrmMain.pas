unit UFrmMain;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms, Dialogs, 
  dxBar, dxRibbon, dxRibbonForm, dxRibbonSkins, cxGraphics, cxControls, cxLookAndFeels,
  cxLookAndFeelPainters, cxClasses, dxRibbonBackstageView, cxBarEditItem,
  dxRibbonCustomizationForm, cxTextEdit, cxContainer, cxEdit, dxSkinsForm,
  dxStatusBar, dxRibbonStatusBar, cxPC, dxSkinsCore, dxSkinBlue,
  dxSkinSharpPlus, dxSkinSilver, dxSkinsdxStatusBarPainter,
  dxSkinsdxDockControlPainter, dxSkinsdxBarPainter, System.Actions,
  Vcl.ActnList, dxDockControl, UFamConnTree, dxDockPanel, Vcl.Menus,
  dxSkinscxPCPainter, dxBarBuiltInMenu, dxTabbedMDI, dxSkinOffice2013White,
  dxSkinWhiteprint;

type
  TFrmMain = class(TdxRibbonForm)
    dxDPConnTree: TdxDockPanel;
    dxDockSite1: TdxDockSite;
    FamConnTree1: TFamConnTree;
    dxDockingManager1: TdxDockingManager;
    dxBar1: TdxBarManager;
    BarManagerBar1: TdxBar;
    BarManagerBar2: TdxBar;
    dxBarButtonLoad: TdxBarLargeButton;
    dxBarButtonSave: TdxBarLargeButton;
    dxBarButtonExit: TdxBarLargeButton;
    siStyles: TdxBarSubItem;
    dxBarButtonStartPage: TdxBarLargeButton;
    dxBarButtonObjectExplorer: TdxBarLargeButton;
    dxBarButtonWatch: TdxBarLargeButton;
    dxBarButtonOutput: TdxBarLargeButton;
    dxBarButtonCallStack: TdxBarLargeButton;
    dxBarButtonProperties: TdxBarLargeButton;
    dxBarButtonClassView: TdxBarLargeButton;
    dxBarButtonSolutionExplorer: TdxBarLargeButton;
    dxBarSubItemOtherWindows: TdxBarSubItem;
    dxBarButton1: TdxBarLargeButton;
    dxBarButton2: TdxBarLargeButton;
    dxBarButton3: TdxBarLargeButton;
    dxBarButton4: TdxBarLargeButton;
    dxBarButton5: TdxBarLargeButton;
    dxBarButton6: TdxBarLargeButton;
    dxBarButton7: TdxBarLargeButton;
    dxBarLargeButton1: TdxBarLargeButton;
    dxBarLargeButton2: TdxBarLargeButton;
    dxBarSubItemFile: TdxBarSubItem;
    dxBarSubItemInsert: TdxBarSubItem;
    dxBarSubItemWindow: TdxBarSubItem;
    siHelp: TdxBarSubItem;
    dxBarButtonDockable: TdxBarButton;
    dxBarButtonHide: TdxBarButton;
    dxBarButtonFloating: TdxBarButton;
    dxBarButtonAutoHide: TdxBarButton;
    ActionList1: TActionList;
    Action1: TAction;
    dxStatusBar1: TdxStatusBar;
    dxBarLIDockStyle: TdxBarListItem;
    dxBPMContext: TdxBarPopupMenu;
    actExit: TAction;
    actAbout: TAction;
    dxDPClient: TdxDockPanel;
    cxTCClient: TcxTabControl;
    dxLayoutDockSite2: TdxLayoutDockSite;
    dxLayoutDockSite3: TdxLayoutDockSite;
    procedure FormCreate(Sender: TObject);
    procedure dxBarLIDockStyleClick(Sender: TObject);
    procedure dxBarButtonObjectExplorerClick(Sender: TObject);
    procedure actExitExecute(Sender: TObject);
    procedure cxTCClientCanCloseEx(Sender: TObject; ATabIndex: Integer;
      var ACanClose: Boolean);
    procedure cxTCClientChange(Sender: TObject);
    procedure dxBarButtonLoadClick(Sender: TObject);
    procedure dxBarButtonSaveClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FrmMain: TFrmMain;

implementation
uses
  UDM, UMods;
{$R *.dfm}

{ TForm1 }

procedure TFrmMain.actExitExecute(Sender: TObject);
begin
  Self.Close;
end;

procedure TFrmMain.cxTCClientCanCloseEx(Sender: TObject; ATabIndex: Integer;
  var ACanClose: Boolean);
var
  aFrm: TForm;
begin
//关闭
  aFrm := TForm(cxTCClient.Tabs.Objects[ATabIndex]);
  if Assigned(aFrm) then
    aFrm.Free;
end;

procedure TFrmMain.cxTCClientChange(Sender: TObject);
var
  aFrm: TObject;
  aIndex: Integer;
begin
  aIndex := cxTCClient.TabIndex;
  if aIndex < 0 then Exit;
  aFrm := cxTCClient.Tabs.Objects[aIndex];
  if not Assigned(aFrm) then Exit;

  cxTCClient.Tabs.BeginUpdate;
  TForm(aFrm).BringToFront;
  cxTCClient.Tabs.EndUpdate;
end;

procedure TFrmMain.dxBarButtonLoadClick(Sender: TObject);
begin
  NewSql('');
end;

procedure TFrmMain.dxBarButtonObjectExplorerClick(Sender: TObject);
begin
//对象资源管理器
  if (dxDPConnTree = nil) then exit;
  if not dxDPConnTree.Visible then
    dxDPConnTree.Visible := True;
  dxDockingController.ActiveDockControl := dxDPConnTree;
end;

procedure TFrmMain.dxBarButtonSaveClick(Sender: TObject);
begin
  EditTable('');
end;

procedure TFrmMain.dxBarLIDockStyleClick(Sender: TObject);
begin
//DockStyle
  if dxBarLIDockStyle.ItemIndex = 1 then
  begin
    dxDockingManager1.DockStyle := dsVS2005;
    dxDockingManager1.Options := dxDockingManager1.Options + [doFillDockingSelection];
  end
  else
  begin
    dxDockingManager1.DockStyle := dsStandard;
    dxDockingManager1.Options := dxDockingManager1.Options - [doFillDockingSelection];
  end;
end;

procedure TFrmMain.FormCreate(Sender: TObject);
begin
  //DisableAero := True;
  //dxDPConnTree.OnContextPopup := dxBPMContext;
  cxTCClient.Tabs.Clear;
  FTabC := cxTCClient;
end;


end.
