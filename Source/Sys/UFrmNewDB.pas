{
数据库链接设置
与PPDAL一起使用。
ini文件
删除/修改 还没做。

[DBconn] //节名
MainConn=SqlServer  当前主数据库链接
SqlServer=........
...
}
unit UFrmNewDB;

interface

uses
  System.Classes, System.SysUtils,
  Vcl.Controls, Vcl.Forms,
  cxButtons, cxVGrid,
  cxPC, cxGraphics, cxControls, cxLookAndFeels, cxLookAndFeelPainters,
  dxBarBuiltInMenu, cxStyles, cxEdit, cxTextEdit, cxDropDownEdit, cxSpinEdit,
  Vcl.Menus, Vcl.ImgList, Vcl.StdCtrls, cxInplaceContainer,
  Vcl.Grids, Vcl.ValEdit, Vcl.ExtCtrls, cxButtonEdit, Vcl.ActnList,
  System.Actions, System.ImageList, Vcl.Dialogs, dxSkinsCore, dxSkinBlue,
  dxSkinSharpPlus, dxSkinSilver, dxSkinscxPCPainter, cxImageComboBox,
  dxSkinOffice2013White, dxSkinWhiteprint;

type
  TFrmNewDB = class(TForm)
    cxVG1: TcxVerticalGrid;
    cxProvider: TcxEditorRow;
    cxButton2: TcxButton;
    cxButton3: TcxButton;
    pnl1: TPanel;
    actl1: TActionList;
    actCreate: TAction;
    Database: TcxEditorRow;
    cxERVersion: TcxEditorRow;
    FileSaveDialog1: TFileSaveDialog;
    actSaveFile: TAction;
    procedure cxButton2Click(Sender: TObject);
    procedure actSaveFileExecute(Sender: TObject);
    procedure actCreateExecute(Sender: TObject);
  private

  public
    class function FrmShow(AProvider: string = 'MSAcc'): Boolean;
  end;

implementation
uses
  UDM, UCommon;
{$R *.dfm}

{ TFrmSetConn }

procedure TFrmNewDB.actCreateExecute(Sender: TObject);
var
  aProvider: string;
  procedure CreateMSAcc;
  begin

  end;
  procedure CreateSqlite;
  begin

  end;
begin
  aProvider := cxProvider.Properties.Value;
  if aProvider = 'MSAcc' then
    CreateMSAcc
  else if aProvider = 'SQLite' then
    CreateSqlite;
end;

procedure TFrmNewDB.actSaveFileExecute(Sender: TObject);
begin
  if FileSaveDialog1.Execute then
    Database.Properties.Value := FileSaveDialog1.FileName;
end;

procedure TFrmNewDB.cxButton2Click(Sender: TObject);
begin
  Self.ModalResult := mrOk;
end;

class function TFrmNewDB.FrmShow(AProvider: string): Boolean;
begin
  with TFrmNewDB.Create(Application) do
  begin
    cxProvider.Properties.Value := AProvider;
    //if True then
    Result := ShowModal = mrOk;
    Free;
  end;
end;


end.
